package com.voiski.toptal.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.voiski.toptal.entity.JogReport;
import com.voiski.toptal.entity.UserAccount;
import com.voiski.toptal.factory.JogRecordFactory;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.test.web.servlet.ResultActions;

import java.time.LocalDateTime;
import java.util.concurrent.TimeUnit;

import static com.voiski.toptal.entity.JogRecord.JogRecordBuilder;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/15/16.
 */
@WebMvcTest(ReportController.class)
public class ReportControllerTest extends AbstractControllerTest {

    @Autowired
    private JogRecordFactory jogRecordFactory;

    @Test
    public void shouldReportWeekJogs() throws Exception {
        // Given I logged as user
        UserAccount login = userAccountFactory.user();
        // And I have five jogs for the week of December 2016
        LocalDateTime date = LocalDateTime.of(2016, 12, 25, 0, 0, 0);
        JogRecordBuilder builder = JogRecordFactory.builder().userAccount(login).distance(1000).time(TimeUnit.MINUTES.toSeconds(5));
        for (int i = 0; i < 5; i++) {
            jogRecordFactory.build(builder.date(date.plusDays(i)));
        }
        // And I have one jog for the previous to last week of December 2016
        jogRecordFactory.build(builder.date(date.minusDays(1)));
        // When I ask for the report for the week of December 2016
        ResultActions result = perform(get("/user/{0}/report/week", login.getUsername()), login);
        // Then It should have the ok status
        result.andExpect(status().isOk());
        // Then I should see the bellow values
        //  | week      | 53               |
        //  | from date | 25 December 2016 |
        //  | to date   | 1 January 2017   |
        //  | distance  | 5000 m           |
        //  | avg speed | 1 km/h           |
        JogReport jogReport = parseJson(result.andReturn(), new TypeReference<JogReport>() {
        });
        assertThat(jogReport.getWeek(), equalTo(53));
        assertThat(jogReport.getFromDate(), equalTo(date.toLocalDate()));
        assertThat(jogReport.getToDate(), equalTo(date.toLocalDate().plusDays(7)));
        assertThat(jogReport.getTotalDistance(), equalTo(5d));
        assertThat(jogReport.getAverageSpeed(), equalTo(12d));
    }
}
