package com.voiski.toptal.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.voiski.toptal.entity.UserAccount;
import com.voiski.toptal.factory.JogRecordFactory;
import com.voiski.toptal.factory.UserAccountFactory;
import com.voiski.toptal.util.APIVersion;
import org.apache.tomcat.util.codec.binary.Base64;
import org.junit.After;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.http.MediaType;
import org.springframework.test.context.junit4.SpringRunner;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;

import java.io.IOException;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/15/16.
 */
@RunWith(SpringRunner.class)
public abstract class AbstractControllerTest {

    @Autowired
    JogRecordFactory jogRecordFactory;

    @Autowired
    UserAccountFactory userAccountFactory;

    @Autowired
    MockMvc mockMvc;

    @Autowired
    ObjectMapper mapper;

    @After
    public void tierDown() {
        jogRecordFactory.clearAll();
        userAccountFactory.clearAll();
    }

    ResultActions perform(MockHttpServletRequestBuilder endPoint, UserAccount login) throws Exception {
        return perform(endPoint, login, null);
    }

    ResultActions perform(MockHttpServletRequestBuilder endPoint, UserAccount login, Object obj) throws Exception {
        endPoint.accept(APIVersion.V1);
        if (login != null) {
            String basicDigestHeaderValue = "Basic " + new String(Base64.encodeBase64((login.getUsername() + ":" + login.getPassword()).getBytes()));
            endPoint.header("Authorization", basicDigestHeaderValue);
        }
        if (obj != null) {
            endPoint.content(mapper.writeValueAsString(obj)).contentType(MediaType.APPLICATION_JSON);
        }
        return mockMvc.perform(endPoint);
    }

    <T> T parseJson(MvcResult result, TypeReference<T> reference) throws IOException {
        return mapper.readValue(
                result.getResponse().getContentAsString(), reference
        );
    }

}
