package com.voiski.toptal.controller;

import com.fasterxml.jackson.core.type.TypeReference;
import com.voiski.toptal.entity.JogRecord;
import com.voiski.toptal.entity.UserAccount;
import com.voiski.toptal.factory.JogRecordFactory;
import com.voiski.toptal.repository.JogRecordRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.web.servlet.ResultActions;

import java.time.LocalDateTime;
import java.util.ArrayList;
import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/15/16.
 */
@WebMvcTest(JogRecordController.class)
public class JogRecordControllerTest extends AbstractControllerTest {

    @Autowired
    private JogRecordRepository jogRecordRepository;

    @Test
    public void shouldCreateNewJog() throws Exception {
        // Given I logged as user
        UserAccount login = userAccountFactory.user();
        // And I have a valid new jog payload
        JogRecord payload = JogRecordFactory.builder().id(null).userAccount(null).build();
        // When I post this new jog for the logged user
        ResultActions result = perform(post("/user/{0}/jog", login.getUsername()), login, payload);
        // Then It should have the created status
        result.andExpect(status().isCreated());
        // Then I should have the original payload persisted
        JogRecord returnedJog = parseJson(result.andReturn(), new TypeReference<JogRecord>() {
        });
        JogRecord expectedReturn = jogRecordFactory.builder(payload).
                jogNumber(returnedJog.getJogNumber()).
                weather(returnedJog.getWeather()).
                build();
        assertThat(returnedJog, equalTo(expectedReturn));
    }

    @Test
    public void shouldNotCreateNewJogForDifferentUser() throws Exception {
        // Given I logged as user
        UserAccount login = userAccountFactory.user();
        // And I have a valid new jog payload
        JogRecord payload = JogRecordFactory.validNew();
        // When I post this new jog for another user
        ResultActions result = perform(post("/user/another_user/jog"), login, payload);
        // Then It should have the forbidden status
        result.andExpect(status().isForbidden());
    }

    @Test
    public void shouldUpdateJogFromLoggedUser() throws Exception {
        // Given I logged as user
        UserAccount login = userAccountFactory.user();
        // And I have a jog entry
        JogRecord jogRecord = jogRecordFactory.today(login);
        // And I have a valid payload to change his distance
        JogRecord payload = JogRecord.builder().distance(1).build();
        // When I put this jog changes
        ResultActions result = perform(
                put("/user/{0}/jog/{1}", login.getUsername(), jogRecord.getJogNumber()),
                login,
                payload
        );
        // Then It should have the ok status
        result.andExpect(status().isOk());
        // Then I should have the jog changes persisted
        jogRecord.setDistance(1);
        result.andExpect(content().string(mapper.writeValueAsString(jogRecord)));
    }

    @Test
    public void shouldNotUpdateJogFromDifferentUser() throws Exception {
        // Given I logged as user
        UserAccount login = userAccountFactory.user();
        // And I have a valid payload to change his distance
        JogRecord payload = jogRecordFactory.today();
        // When I put this jog changes
        ResultActions result = perform(
                put("/user/another_user/jog/{0}", payload.getJogNumber()),
                login,
                payload
        );
        // Then It should have the forbidden status
        result.andExpect(status().isForbidden());
    }

    @Test
    public void shouldDeleteJogFromLoggedUser() throws Exception {
        // Given I logged as user
        UserAccount login = userAccountFactory.user();
        // And I have one jog for this user
        JogRecord jogFromLoggedUser = jogRecordFactory.today(login);
        // When I delete this jog
        ResultActions result = perform(
                delete("/user/{0}/jog/{1}", login.getUsername(), jogFromLoggedUser.getJogNumber()),
                login,
                null
        );
        // Then It should have the no content status
        result.andExpect(status().isNoContent());
        // And The database should not have this user anymore
        assertThat(jogRecordRepository.findOne(jogFromLoggedUser.getId()), nullValue());
    }

    @Test
    public void shouldNotDeleteJogFromDifferentUser() throws Exception {
        // Given I logged as user
        UserAccount login = userAccountFactory.user();
        // And I have one jog for another use
        JogRecord jogFromLoggedUser = jogRecordFactory.today();
        // When I delete this jog
        ResultActions result = perform(
                delete("/user/another_user/jog/{0}", jogFromLoggedUser.getJogNumber()),
                login,
                null
        );
        // Then It should have the forbidden status
        result.andExpect(status().isForbidden());
    }

    @Test
    public void shouldIndexWithTheObjectsFromPageTwo() throws Exception {
        // Given I logged as manager user
        UserAccount login = userAccountFactory.user();
        // And I have 20 jog for this user
        List<JogRecord> jogs = new ArrayList<>(10);
        for (int i = 0; i < 20; i++) {
            JogRecord jogRecord = jogRecordFactory.today(login);
            if (i > 9) {
                jogs.add(jogRecord);
            }
        }
        // When I index page 2 and with 10 registries per page
        ResultActions result = perform(get("/user/{0}/jog?page=1&size=10", login.getUsername()), login, null);
        // Then It should have the ok status
        result.andExpect(status().isOk());
        // And The result list should be on the page 2 with the last jogs from the database
        Page<JogRecord> expectedReturn = new PageImpl<>(jogs, new PageRequest(1, 10), 20);
        result.andExpect(content().string(mapper.writeValueAsString(expectedReturn)));
    }

    @Test
    public void shouldIndexWithSampleQuery() throws Exception {
        // Given I logged as manager user
        UserAccount login = userAccountFactory.user();
        // And I have the follow jogs
        // | distance | date       |
        // | 20       | 2016-05-01 |
        // | 21       | 2016-05-02 |
        // | 21       | 2016-05-01 |
        // | 9        | 2016-05-01 |
        jogRecordFactory.build(jogRecordFactory.builder().distance(20).userAccount(login).date(LocalDateTime.of(2016,5,1,12,0,0)));
        jogRecordFactory.build(jogRecordFactory.builder().distance(20).userAccount(login).date(LocalDateTime.of(2016,5,2,12,0,0)));
        JogRecord recordWithDistance21 = jogRecordFactory.build(jogRecordFactory.builder().distance(21).userAccount(login).date(LocalDateTime.of(2016,5,1,12,0,0)));
        JogRecord recordWithDistance9 = jogRecordFactory.build(jogRecordFactory.builder().distance(9).userAccount(login).date(LocalDateTime.of(2016,5,1,12,0,0)));
        // When I index by query (date eq '2016-05-01') AND ((distance gt 20) OR (distance lt 10))
        String query = "(date eq '2016-05-01') AND ((distance gt 20) OR (distance lt 10))";
        ResultActions result = perform(get("/user/{0}/jog?query={1}", login.getUsername(), query), login, null);
        // Then It should have the ok status
        result.andExpect(status().isOk());
        // And The result list should have two jogs from day 2016-05-01 with distance 9 or 21
        result.andExpect(content().string(containsString(mapper.writeValueAsString(Arrays.asList(
                recordWithDistance21,
                recordWithDistance9
        )))));
    }

    @Test
    public void shouldShowJogsFromLoggedUser() throws Exception {
        // Given I logged as user
        UserAccount login = userAccountFactory.user();
        // And I have one jog for this user
        JogRecord jogFromLoggedUser = jogRecordFactory.today(login);
        // When I show the jog
        ResultActions result = perform(
                get("/user/{0}/jog/{1}", login.getUsername(), jogFromLoggedUser.getJogNumber()),
                login,
                null
        );
        // Then It should have the ok status
        result.andExpect(status().isOk());
        // Then I should show the jog information
        result.andExpect(content().string(mapper.writeValueAsString(jogFromLoggedUser)));
    }

    @Test
    public void shouldNotShowJogsFromDifferentUser() throws Exception {
        // Given I logged as user
        UserAccount login = userAccountFactory.user();
        // And I have one jog for another user
        JogRecord jogFromLoggedUser = jogRecordFactory.today();
        // When I show the jog
        ResultActions result = perform(
                get("/user/another_user/jog/{0}", jogFromLoggedUser.getJogNumber()),
                login,
                null
        );
        // Then It should have the forbidden status
        result.andExpect(status().isForbidden());
    }

}
