package com.voiski.toptal.controller;

import com.voiski.toptal.entity.UserAccount;
import com.voiski.toptal.entity.UserRoles;
import com.voiski.toptal.factory.UserAccountFactory;
import com.voiski.toptal.repository.UserAccountRepository;
import org.junit.Test;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.autoconfigure.web.servlet.WebMvcTest;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.test.web.servlet.ResultActions;

import java.util.ArrayList;
import java.util.Collections;
import java.util.List;

import static org.hamcrest.Matchers.containsString;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/15/16.
 */
@WebMvcTest(UserAccountController.class)
public class UserAccountControllerTest extends AbstractControllerTest {

    @Autowired
    private UserAccountRepository userAccountRepository;

    @Test
    public void shouldCreateNewUser() throws Exception {
        // Given I not logged
        UserAccount login = null;
        // And I have a valid payload
        UserAccount payload = UserAccountFactory.validNew();
        payload.setRoles(null);
        // When I post this new user
        ResultActions result = perform(post("/user"), login, UserAccountFactory.toBase(payload));
        // Then It should have the created status
        result.andExpect(status().isCreated());
        // Then I should have the original payload persisted with user role
        payload.setRoles(Collections.singletonList(UserRoles.USER));
        result.andExpect(content().string(mapper.writeValueAsString(payload)));
    }

    @Test
    public void shouldUpdateOtherUserPasswordIfLoggedAdmin() throws Exception {
        // Given I logged as admin user
        UserAccount login = userAccountFactory.admin();
        // And I have a valid payload to change the password from a different user
        UserAccount payload = userAccountFactory.builder(userAccountFactory.user()).password("newP@55word").build();
        // When I put this changes
        ResultActions result = perform(put("/user/" + payload.getUsername()), login, UserAccountFactory.toBase(payload));
        // Then It should have the ok status
        result.andExpect(status().isOk());
        // Then I should have the original payload persisted
        result.andExpect(content().string(mapper.writeValueAsString(payload)));
    }

    @Test
    public void shouldUpdateOtherUserPasswordIfLoggedManager() throws Exception {
        // Given I logged as manager user
        UserAccount login = userAccountFactory.manager();
        // And I have a valid payload to change the password from a different user
        UserAccount payload = userAccountFactory.builder(userAccountFactory.user()).password("newP@55word").build();
        // When I put this changes
        ResultActions result = perform(put("/user/" + payload.getUsername()), login, UserAccountFactory.toBase(payload));
        // Then It should have the ok status
        result.andExpect(status().isOk());
        // Then I should have the original payload persisted
        result.andExpect(content().string(mapper.writeValueAsString(payload)));
    }

    @Test
    public void shouldUpdateLoggedUserPassword() throws Exception {
        // Given I logged as user
        UserAccount login = userAccountFactory.user();
        // And I have a valid payload to change his password
        UserAccount payload = UserAccount.builder().password("newP@55word").build();
        // When I put this changes
        ResultActions result = perform(put("/user/" + login.getUsername()), login, UserAccountFactory.toBase(payload));
        // Then It should have the ok status
        result.andExpect(status().isOk());
    }

    @Test
    public void shouldNotUpdateDifferentLoggedUserPassword() throws Exception {
        // Given I logged as user
        UserAccount login = userAccountFactory.user();
        // And I have a valid payload to change the password from another user
        UserAccount payload = userAccountFactory.builder(userAccountFactory.user()).password("newP@55word").build();
        // When I put this changes
        ResultActions result = perform(put("/user/" + payload.getUsername()), login, UserAccountFactory.toBase(payload));
        // Then It should have the forbidden status
        result.andExpect(status().isForbidden());
    }

    @Test
    public void shouldDeleteOwnUser() throws Exception {
        // Given I logged as user
        UserAccount login = userAccountFactory.user();
        // When I delete the own user
        ResultActions result = perform(delete("/user/" + login.getUsername()), login, null);
        // Then It should have the no content status
        result.andExpect(status().isNoContent());
        // And The database should not have this user anymore
        assertThat(userAccountRepository.findOne(login.getId()), nullValue());
    }

    @Test
    public void shouldNotDeleteDifferentUser() throws Exception {
        // Given I logged as user
        UserAccount login = userAccountFactory.user();
        // When I delete a different user
        ResultActions result = perform(delete("/user/" + userAccountFactory.user().getUsername()), login, null);
        // Then It should have the forbidden status
        result.andExpect(status().isForbidden());
    }

    @Test
    public void shouldNotIndexIfNormalUser() throws Exception {
        // Given I logged as user
        UserAccount login = userAccountFactory.user();
        // When I index
        ResultActions result = perform(get("/user"), login, null);
        // Then It should have the forbidden status
        result.andExpect(status().isForbidden());
    }

    @Test
    public void shouldIndexAsManager() throws Exception {
        // Given I logged as manager user
        UserAccount login = userAccountFactory.manager();
        // When I index
        ResultActions result = perform(get("/user"), login, null);
        // Then It should have the ok status
        result.andExpect(status().isOk());
        // And The result list should have one user
        result.andExpect(content().string(
                containsString(mapper.writeValueAsString(Collections.singletonList(login)))
        ));
    }

    @Test
    public void shouldIndexWithTheObjectsFromPageTwo() throws Exception {
        // Given I logged as manager user
        UserAccount login = userAccountFactory.manager();
        // And I have 20 users in the database
        List<UserAccount> users = new ArrayList<>(10);
        for (int i = 0; i < 19; i++) {
            UserAccount user = userAccountFactory.user();
            if (i > 8) {
                users.add(user);
            }
        }
        // When I index page 2 and with 10 registries per page
        ResultActions result = perform(get("/user?page=1&size=10"), login, null);
        // Then It should have the ok status
        result.andExpect(status().isOk());
        // And The result list should be on the page 2 with the last users from the database
        Page<UserAccount> expectedReturn = new PageImpl<>(users, new PageRequest(1, 10), 20);
        result.andExpect(content().string(mapper.writeValueAsString(expectedReturn)));
    }

    @Test
    public void shouldIndexQueryByGreaterThenLoggedUserUsername() throws Exception {
        // Given I logged as manager user
        UserAccount login = userAccountFactory.manager();
        // And I have 2 users in the database
        UserAccount seccondUser = userAccountFactory.user();
        // When I index page 2 and with 10 registries per page
        ResultActions result = perform(get("/user?query=(username gt '"+ login.getUsername() +"')"), login, null);
        // Then It should have the ok status
        result.andExpect(status().isOk());
        // And The result list should have only the other user
        result.andExpect(content().string(
                containsString(mapper.writeValueAsString(Collections.singletonList(seccondUser)))
        ));
    }

    @Test
    public void shouldShowOwnUser() throws Exception {
        // Given I logged as user
        UserAccount login = userAccountFactory.user();
        // When I show the own user
        ResultActions result = perform(get("/user/" + login.getUsername()), login, null);
        // Then It should have the ok status
        result.andExpect(status().isOk());
        // Then I should show the information of the logged user
        result.andExpect(content().string(mapper.writeValueAsString(login)));
    }

    @Test
    public void shouldNotShowDifferentUser() throws Exception {
        // Given I logged as user
        UserAccount login = userAccountFactory.user();
        // When I show a different user
        ResultActions result = perform(get("/user/" + userAccountFactory.user().getUsername()), login, null);
        // Then It should have the forbidden status
        result.andExpect(status().isForbidden());
    }

}
