package com.voiski.toptal.client;

import com.voiski.toptal.entity.Weather;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.boot.test.context.SpringBootTest;
import org.springframework.test.context.junit4.SpringRunner;

import static org.hamcrest.Matchers.notNullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/16/16.
 */
@RunWith(SpringRunner.class)
@SpringBootTest
public class OpenWeatherClientTest {

    @Autowired
    private OpenWeatherClient client;

    @Test
    public void testWeather() {
        Weather weather = client.getWeather(-19.93, -43.94);
        assertThat(weather.getClimate(), notNullValue());
        assertThat(weather.getHumidity(), notNullValue());
        assertThat(weather.getWindSpeed(), notNullValue());
        assertThat(weather.getDescription(), notNullValue());
    }
}
