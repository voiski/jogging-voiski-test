package com.voiski.toptal.repository;

import com.querydsl.core.types.Predicate;
import com.voiski.toptal.config.FongoConfig;
import com.voiski.toptal.entity.JogRecord;
import com.voiski.toptal.entity.QUserAccount;
import com.voiski.toptal.entity.UserAccount;
import com.voiski.toptal.factory.UserAccountFactory;
import com.voiski.toptal.repository.query.QueryBuilder;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {FongoConfig.class})
public class UserAccountRepositoryTest {

    @Autowired
    private UserAccountRepository repository;

    @Autowired
    private MongoTemplate mongoTemplate;

    @After
    public void tierDown() {
        mongoTemplate.findAllAndRemove(new Query(), JogRecord.class);
        mongoTemplate.findAllAndRemove(new Query(), UserAccount.class);
    }

    @Test
    public void testCreate() {
        UserAccount user = repository.save(UserAccountFactory.validNew());
        assertThat(user.getId(), notNullValue());
    }

    @Test(expected = DuplicateKeyException.class)
    public void testCreateDuplicatedUsername() {
        repository.save(UserAccountFactory.builder().username("sameUser").build());
        repository.save(UserAccountFactory.builder().username("sameUser").build());
    }

    @Test
    public void testFindByUsername() {
        UserAccount userSample = repository.save(UserAccountFactory.validNew());
        UserAccount user = repository.findByUsername(userSample.getUsername());
        assertThat(user, equalTo(userSample));
    }

    @Test
    public void testUpdate() {
        UserAccount userSample = repository.save(UserAccountFactory.validNew());
        UserAccount userToEdit = repository.findOne(userSample.getId());
        userToEdit.setUsername("changed");
        userToEdit = repository.save(userToEdit);
        assertThat(userToEdit, not(equalTo(userSample)));
        assertThat(repository.findByUsername("changed"), equalTo(userToEdit));
    }

    @Test
    public void testDelete() {
        UserAccount userSample = repository.save(UserAccountFactory.validNew());
        repository.delete(userSample.getId());
        assertThat(repository.findByUsername(userSample.getUsername()), nullValue());
    }

    @Test
    public void testGetByQuery() {
        UserAccount userSample = repository.save(UserAccountFactory.validNew());
        Predicate predicate = QueryBuilder.create(QUserAccount.userAccount).build("(username eq '" + userSample.getUsername() + "')");
        Iterable<UserAccount> list = repository.findAll(predicate);
        assertThat(list.iterator().next(), equalTo(userSample));
    }
}
