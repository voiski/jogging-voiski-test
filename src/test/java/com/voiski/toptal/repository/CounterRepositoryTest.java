package com.voiski.toptal.repository;

import com.voiski.toptal.config.FongoConfig;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.util.Arrays;
import java.util.List;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {FongoConfig.class})
public class CounterRepositoryTest {

    @Autowired
    private CounterRepository counterRepository;

    @Test
    public void testFindNextSequence() {
        List<Integer> result = Arrays.asList(
                counterRepository.findNextSequence("alan"),
                counterRepository.findNextSequence("alan"),
                counterRepository.findNextSequence("alan"),
                counterRepository.findNextSequence("alan_2")
        );
        assertThat(result, equalTo(Arrays.asList(1, 2, 3, 1)));
    }
}
