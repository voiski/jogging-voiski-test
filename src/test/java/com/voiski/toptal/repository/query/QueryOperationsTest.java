package com.voiski.toptal.repository.query;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.voiski.toptal.factory.EntityPathFactory;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/21/16.
 */
public class QueryOperationsTest {

    @Test
    public void testEqual() {
        BooleanExpression criteria = QueryOperations.eq(EntityPathFactory.base.stringPath, "Sample");
        assertThat(criteria.toString(), equalTo("base.string = Sample"));
    }

    @Test
    public void testEqualDatetime() {
        BooleanExpression criteria = QueryOperations.eq(EntityPathFactory.base.datePath, "2016-12-21 10:10:10");
        assertThat(criteria.toString(), equalTo("base.date = 2016-12-21T10:10:10"));
    }

    @Test
    public void testEqualDate() {
        BooleanExpression criteria = QueryOperations.eq(EntityPathFactory.base.datePath, "2016-12-21");
        assertThat(criteria.toString(), equalTo("base.date >= 2016-12-21T00:00 && base.date < 2016-12-22T00:00"));
    }

    @Test
    public void testEqualDouble() {
        BooleanExpression criteria = QueryOperations.eq(EntityPathFactory.base.doublePath, "20.20");
        assertThat(criteria.toString(), equalTo("base.double = 20.2"));
    }

    @Test
    public void testEqualLong() {
        BooleanExpression criteria = QueryOperations.eq(EntityPathFactory.base.longPath, "32131231321321312");
        assertThat(criteria.toString(), equalTo("base.long = 32131231321321312"));
    }

    @Test
    public void testNotEqual() {
        BooleanExpression criteria = QueryOperations.ne(EntityPathFactory.base.stringPath, "Sample");
        assertThat(criteria.toString(), equalTo("base.string != Sample"));
    }

    @Test
    public void testGreater() {
        BooleanExpression criteria = QueryOperations.gt(EntityPathFactory.base.stringPath, "Sample");
        assertThat(criteria.toString(), equalTo("base.string > Sample"));
    }

    @Test
    public void testGreaterNumber() {
        BooleanExpression criteria = QueryOperations.gt(EntityPathFactory.base.integerPath, "123");
        assertThat(criteria.toString(), equalTo("base.integer > 123"));
    }

    @Test
    public void testGreaterEquals() {
        BooleanExpression criteria = QueryOperations.goe(EntityPathFactory.base.stringPath, "Sample");
        assertThat(criteria.toString(), equalTo("base.string >= Sample"));
    }

    @Test
    public void testGreaterEqualsNumber() {
        BooleanExpression criteria = QueryOperations.goe(EntityPathFactory.base.integerPath, "123");
        assertThat(criteria.toString(), equalTo("base.integer >= 123"));
    }

    @Test
    public void testLess() {
        BooleanExpression criteria = QueryOperations.lt(EntityPathFactory.base.stringPath, "Sample");
        assertThat(criteria.toString(), equalTo("base.string < Sample"));
    }

    @Test
    public void testLessNumber() {
        BooleanExpression criteria = QueryOperations.lt(EntityPathFactory.base.integerPath, "123");
        assertThat(criteria.toString(), equalTo("base.integer < 123"));
    }

    @Test
    public void testLessEquals() {
        BooleanExpression criteria = QueryOperations.loe(EntityPathFactory.base.stringPath, "Sample");
        assertThat(criteria.toString(), equalTo("base.string <= Sample"));
    }

    @Test
    public void testLessEqualsNumber() {
        BooleanExpression criteria = QueryOperations.loe(EntityPathFactory.base.integerPath, "123");
        assertThat(criteria.toString(), equalTo("base.integer <= 123"));
    }
}
