package com.voiski.toptal.repository.query;

import com.querydsl.core.types.Predicate;
import com.voiski.toptal.factory.EntityPathFactory;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/21/16.
 */
public class QueryBuilderTest {

    private QueryBuilder query = new QueryBuilder(EntityPathFactory.base);

    @Test
    public void testBuildSimple() {
        Predicate predicate = query.build("(stringPath eq 'Sample')");
        assertThat(predicate.toString(), equalTo("base.string = Sample"));
    }

    @Test
    public void testBuildSimpleGroup() {
        Predicate predicate = query.build("(stringPath eq 'Sample') AND (longPath goe 123)");
        assertThat(predicate.toString(), equalTo("base.string = Sample && base.long >= 123"));
    }

    @Test
    public void testBuildComplexGroup() {
        Predicate predicate = query.build("(stringPath eq 'Sample') AND ((longPath goe 123) OR (integerPath goe 123))");
        assertThat(predicate.toString(), equalTo("base.string = Sample && (base.long >= 123 || base.integer >= 123)"));
    }
}
