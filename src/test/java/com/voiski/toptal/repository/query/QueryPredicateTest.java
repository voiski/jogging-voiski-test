package com.voiski.toptal.repository.query;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.voiski.toptal.exception.InvalidQueryException;
import com.voiski.toptal.factory.EntityPathFactory;
import org.junit.Test;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/21/16.
 */
public class QueryPredicateTest {

    private QueryPredicate query = new QueryPredicate(EntityPathFactory.base);

    @Test
    public void testGetPredicate() {
        BooleanExpression criteria = query.getPredicate("stringPath", "eq", "Sample");
        assertThat(criteria.toString(), equalTo("base.string = Sample"));
    }

    @Test(expected = InvalidQueryException.class)
    public void testGetPredicateWrongField() {
        query.getPredicate("wrongPath", "eq", "Sample");
    }

    @Test(expected = InvalidQueryException.class)
    public void testGetPredicateWrongOperation() {
        query.getPredicate("stringPath", "wrong", "Sample");
    }

    @Test
    public void testProcessPredicate() {
        BooleanExpression criteria = query.processCriteria("datePath eq '2016-12-21'");
        assertThat(criteria.toString(), equalTo("base.date >= 2016-12-21T00:00 && base.date < 2016-12-22T00:00"));
    }

    @Test(expected = InvalidQueryException.class)
    public void testProcessBadPredicate() {
        query.processCriteria("datePath equal '2016-12-21'");
    }
}
