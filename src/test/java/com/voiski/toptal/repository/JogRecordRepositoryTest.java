package com.voiski.toptal.repository;

import com.querydsl.core.types.Predicate;
import com.querydsl.core.types.dsl.BooleanExpression;
import com.voiski.toptal.config.FongoConfig;
import com.voiski.toptal.entity.JogRecord;
import com.voiski.toptal.entity.JogRecord.JogRecordBuilder;
import com.voiski.toptal.entity.QJogRecord;
import com.voiski.toptal.entity.UserAccount;
import com.voiski.toptal.factory.JogRecordFactory;
import com.voiski.toptal.repository.query.QueryBuilder;
import com.voiski.toptal.util.DateUtil;
import lombok.val;
import org.junit.After;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.test.context.ContextConfiguration;
import org.springframework.test.context.junit4.SpringJUnit4ClassRunner;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 */
@RunWith(SpringJUnit4ClassRunner.class)
@ContextConfiguration(classes = {FongoConfig.class})
public class JogRecordRepositoryTest {

    @Autowired
    private JogRecordRepository repository;

    @Autowired
    private UserAccountRepository repositoryUserAccount;

    @Autowired
    private MongoTemplate mongoTemplate;

    @After
    public void tierDown() {
        mongoTemplate.findAllAndRemove(new Query(), JogRecord.class);
        mongoTemplate.findAllAndRemove(new Query(), UserAccount.class);
    }

    @Test
    public void testCreate() {
        JogRecord jogRecord = repository.save(JogRecordFactory.validNew());
        assertThat(jogRecord.getId(), notNullValue());
    }

    @Test(expected = DuplicateKeyException.class)
    public void testCreateDuplicatedUserJogNumber() {
        JogRecord firstJog = repository.save(JogRecordFactory.validNew());
        JogRecord secondJog = repository.save(
                JogRecordFactory.builder().
                        userAccount(firstJog.getUserAccount()).
                        jogNumber(firstJog.getJogNumber()).
                        build()
        );
    }

    @Test
    public void testUpdate() {
        JogRecord jogRecordSample = createJog();
        JogRecord jogRecordToEdit = repository.findOne(jogRecordSample.getId());
        jogRecordToEdit.setLocation(Arrays.asList(new Double[]{10d, 10d}));
        jogRecordToEdit = repository.save(jogRecordToEdit);
        assertThat(jogRecordToEdit, not(equalTo(jogRecordSample)));
        assertThat(repository.findOne(jogRecordToEdit.getId()), equalTo(jogRecordToEdit));
    }

    @Test
    public void testDelete() {
        JogRecord jogRecordSample = createJog();
        repository.delete(jogRecordSample.getId());
        assertThat(repository.findOne(jogRecordSample.getId()), nullValue());
    }

    @Test
    public void testFindByUsername() {
        JogRecord jogRecordSample = createJog();

        val jogs = repository.findByUserAccount(
                jogRecordSample.getUserAccount(),
                new PageRequest(0, 1)
        );
        assertThat(jogs.iterator().next(), equalTo(jogRecordSample));
    }

    @Test
    public void testFindAllWithQuery() {
        build(JogRecordFactory.builder().distance(20).date(LocalDateTime.of(2016, 5, 1, 12, 0, 0)));
        build(JogRecordFactory.builder().distance(21).date(LocalDateTime.of(2016, 5, 2, 12, 0, 0)));
        JogRecord recordWithDistance21 = build(JogRecordFactory.builder().distance(21).date(LocalDateTime.of(2016, 5, 1, 12, 0, 0)));
        JogRecord recordWithDistance9 = build(JogRecordFactory.builder().distance(9).date(LocalDateTime.of(2016, 5, 1, 12, 0, 0)));

        QueryBuilder queryBuilder = new QueryBuilder(QJogRecord.jogRecord);
        Predicate query = queryBuilder.build("(date eq '2016-05-01') AND ((distance gt 20) OR (distance lt 10))");
        val jogs = repository.findAll(query, new PageRequest(0, 10));
        assertThat(jogs.getContent(), equalTo(Arrays.asList(
                recordWithDistance21,
                recordWithDistance9
        )));
    }

    @Test
    public void testFindByUsernameAndJogNumber() {
        JogRecord jogRecordSample = createJog();

        JogRecord jog = repository.findByUserAccountAndJogNumber(jogRecordSample.getUserAccount(),
                jogRecordSample.getJogNumber());
        assertThat(jog, equalTo(jogRecordSample));
    }

    @Test
    public void testGetByQuery() {
        JogRecord jogRecordSample = createJog();

        BooleanExpression usernameCriteria = QJogRecord.jogRecord.
                userAccount.eq(jogRecordSample.getUserAccount());

        BooleanExpression queryCriteria = QueryBuilder.create(QJogRecord.jogRecord).
                build(String.format("(date eq '%s') AND ((distance gt %d) OR (distance lt %d))",
                        jogRecordSample.getDate().format(DateUtil.FORMATTER_DATETIME),
                        jogRecordSample.getDistance() - 1,
                        jogRecordSample.getDistance() + 1
                ));

        Iterable<JogRecord> list = repository.findAll(usernameCriteria.and(queryCriteria));
        assertThat(list.iterator().next(), equalTo(jogRecordSample));
    }

    private JogRecord build(JogRecordBuilder builder){
        JogRecord jogRecordSample = builder.build();
        jogRecordSample.setUserAccount(repositoryUserAccount.save(jogRecordSample.getUserAccount()));
        jogRecordSample = repository.save(jogRecordSample);
        return jogRecordSample;
    }

    private JogRecord createJog() {
        JogRecord jogRecordSample = JogRecordFactory.validNew();
        jogRecordSample.setUserAccount(repositoryUserAccount.save(jogRecordSample.getUserAccount()));
        jogRecordSample = repository.save(jogRecordSample);
        return jogRecordSample;
    }
}
