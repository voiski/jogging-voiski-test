package com.voiski.toptal.exception;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.voiski.toptal.util.DateUtil;
import com.voiski.toptal.util.Messages;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotEmpty;
import org.junit.Before;
import org.junit.BeforeClass;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.support.StaticApplicationContext;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.stereotype.Controller;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.bind.annotation.*;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import javax.validation.Valid;
import javax.validation.constraints.Max;
import java.io.IOException;
import java.io.Serializable;
import java.text.MessageFormat;
import java.time.LocalDateTime;

import static org.hamcrest.CoreMatchers.allOf;
import static org.hamcrest.CoreMatchers.containsString;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.post;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/23/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class GlobalExceptionHandlerTest {

    public static final String SAMPLE_PATH = "/sample";
    private static SampleEntity sample;
    private MockMvc mockMvc;
    private static ObjectMapper mapper;

    @Data
    @Builder
    @AllArgsConstructor
    @NoArgsConstructor
    public static class SampleEntity implements Serializable {
        @NotEmpty(message = "field one cannot be empty")
        protected String one;

        @Max(message = "field two is invalid", value = 3000)
        protected Integer two;
    }

    @Controller
    @RequestMapping(SAMPLE_PATH)
    public static class SampleController {

        @RequestMapping(method = RequestMethod.POST)
        @ResponseBody
        public ResponseEntity<?> post(@Valid @RequestBody final SampleEntity entity) {
            return null;
        }

        @RequestMapping(method = RequestMethod.GET)
        @ResponseBody
        public ResponseEntity<?> get(@RequestParam(value = "date") @DateTimeFormat(pattern = DateUtil.PATTERN) LocalDateTime date) {
            return null;
        }
    }

    @Mock
    private SampleController controller;

    @BeforeClass
    public static void initClass() {
        mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
        sample = new SampleEntity("test", 1);
    }

    @Before
    public void setUp() throws Exception {
        final StaticApplicationContext applicationContext = new StaticApplicationContext();
        applicationContext.registerSingleton("exceptionHandler", GlobalExceptionHandler.class);

        final WebMvcConfigurationSupport webMvcConfigurationSupport = new WebMvcConfigurationSupport();
        webMvcConfigurationSupport.setApplicationContext(applicationContext);

        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(controller)
                .setHandlerExceptionResolvers(webMvcConfigurationSupport.handlerExceptionResolver()).build();
    }

    @Test
    public void shouldHandleIOException() throws Exception {
        when(controller.post(any(SampleEntity.class))).thenThrow(IOException.class);

        perform(post(SAMPLE_PATH), mapper.writeValueAsString(sample))
                .andExpect(status().isNotFound());
    }

    @Test
    public void shouldHandleServiceException() throws Exception {
        when(controller.post(any(SampleEntity.class))).thenThrow(new ServiceException("Not a nice exception!"));

        perform(post(SAMPLE_PATH), mapper.writeValueAsString(sample))
                .andExpect(status().isInternalServerError())
                .andExpect(content().string(containsString("Not a nice exception!")));
    }

    @Test
    public void shouldHandleInvalidSearchCriteriaException() throws Exception {
        when(controller.post(any(SampleEntity.class))).thenThrow(new InvalidQueryException("Invalid search!"));

        perform(post(SAMPLE_PATH), mapper.writeValueAsString(sample))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("Invalid search!")));
    }

    @Test
    public void shouldHandleNoResultServiceException() throws Exception {
        when(controller.post(any(SampleEntity.class))).thenThrow(new NoResultServiceException("Not found!"));

        perform(post(SAMPLE_PATH), mapper.writeValueAsString(sample))
                .andExpect(status().isNotFound())
                .andExpect(content().string(containsString("Not found!")));
    }

    @Test
    public void shouldHandleForbiddenFieldChangeException() throws Exception {
        when(controller.post(any(SampleEntity.class))).thenThrow(new ForbiddenFieldChangeException("one"));

        perform(post(SAMPLE_PATH), mapper.writeValueAsString(sample))
                .andExpect(status().isForbidden())
                .andExpect(content().string(containsString(
                        MessageFormat.format(Messages.FORBIDDEN_FIELD_CHANGE, "one")
                )));
    }

    @Test
    public void shouldHandleValidationError() throws Exception {
        perform(post(SAMPLE_PATH), "{\"two\":3001}")
                .andExpect(status().isBadRequest())
                .andExpect(content()
                        .string(allOf(containsString("one: field one cannot be empty"),
                                containsString("two: field two is invalid"))));
    }

    @Test
    public void shouldHandleTypeMismatch() throws Exception {
        perform(get("{0}?date=20161301000000", SAMPLE_PATH), null)
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("Wrong format at date")));
    }

    @Test
    public void shouldHandleMissingArgument() throws Exception {
        perform(get(SAMPLE_PATH), null)
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString("Missing parameter date")));
    }

    @Test
    public void shouldHandleDuplicateKeyException() throws Exception {
        when(controller.post(any(SampleEntity.class))).thenThrow(new DuplicateKeyException(
                "E11000 duplicate key error collection: toptal-voiski-test.userAccount index: one_and_two dup key: { : alanvoiski }'"
        ));

        perform(post(SAMPLE_PATH), mapper.writeValueAsString(sample))
                .andExpect(status().isBadRequest())
                .andExpect(content().string(containsString(
                        MessageFormat.format(Messages.DUPLICATED_ENTRY, "one and two")
                )));
    }

    private ResultActions perform(MockHttpServletRequestBuilder endPoint, Object obj) throws Exception {
        endPoint.accept(MediaType.APPLICATION_JSON);
        if (obj != null) {
            endPoint.content(obj instanceof String ? (String) obj : mapper.writeValueAsString(obj))
                    .contentType(MediaType.APPLICATION_JSON);
        }
        return mockMvc.perform(endPoint);
    }
}
