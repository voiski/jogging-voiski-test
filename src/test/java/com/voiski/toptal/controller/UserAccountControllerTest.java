package com.voiski.toptal.controller;

import com.voiski.toptal.entity.BaseUserAccount;
import com.voiski.toptal.entity.UserAccount;
import com.voiski.toptal.entity.UserRoles;
import com.voiski.toptal.factory.UserAccountFactory;
import com.voiski.toptal.service.UserAccountService;
import com.voiski.toptal.util.Messages;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;
import org.springframework.security.core.authority.SimpleGrantedAuthority;

import java.text.MessageFormat;
import java.util.Arrays;
import java.util.Collections;

import static org.hamcrest.Matchers.allOf;
import static org.hamcrest.Matchers.containsString;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.doReturn;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/15/16.
 */
public class UserAccountControllerTest extends AbstractControllerTest {

    @InjectMocks
    private UserAccountController userAccountController;

    @Mock
    private UserAccountService userAccountService;

    @Before
    public void init() {
        init(userAccountController);
    }

    @Test
    public void testCreate() throws Exception {
        UserAccount sample = UserAccountFactory.builder().roles(null).build();
        perform(post("/user"), UserAccountFactory.toBase(sample)).
                andExpect(status().isCreated()).
                andExpect(content().json(mapper.writeValueAsString(sample)));
    }

    @Test
    public void testCreateRoleForbidden() throws Exception {
        UserAccount sample = UserAccountFactory.validNew();
        perform(post("/user"), UserAccountFactory.toBase(sample)).
                andExpect(status().isForbidden()).
                andExpect(content().string(containsString(
                        MessageFormat.format(Messages.FORBIDDEN_FIELD_CHANGE, "roles")
                )));
    }

    @Test
    public void testCreateRolePermitted() throws Exception {
        doReturn(Collections.singletonList(new SimpleGrantedAuthority("ROLE_".concat(UserRoles.MANAGER)))).
                when(authentication).
                getAuthorities();

        UserAccount sample = UserAccountFactory.validNew();
        perform(post("/user"), UserAccountFactory.toBase(sample)).
                andExpect(status().isCreated()).
                andExpect(content().json(mapper.writeValueAsString(sample)));
    }

    @Test
    public void testCreateMissingFields() throws Exception {
        perform(post("/user"), new UserAccount()).
                andExpect(status().isBadRequest()).
                andExpect(content().string(allOf(
                        containsString("username: may not be empty"),
                        containsString("password: may not be empty")
                )));
    }

    @Test
    public void testUpdate() throws Exception {
        UserAccount sample = UserAccountFactory.valid();
        when(userAccountService.getByUsername(anyString())).thenReturn(sample);
        perform(put("/user/alan"),
                UserAccount.builder().password(sample.getPassword()).build()).
                andExpect(status().isOk()).
                andExpect(content().json(mapper.writeValueAsString(sample)));
    }

    @Test
    public void testUpdateRoleForbidden() throws Exception {
        UserAccount sample = UserAccountFactory.validNew();
        perform(put("/user/alan"), sample).
                andExpect(status().isForbidden()).
                andExpect(content().string(containsString(
                        MessageFormat.format(Messages.FORBIDDEN_FIELD_CHANGE, "roles")
                )));
    }

    @Test
    public void testUpdateRolePermitted() throws Exception {
        doReturn(Collections.singletonList(new SimpleGrantedAuthority("ROLE_".concat(UserRoles.MANAGER)))).
                when(authentication).
                getAuthorities();

        UserAccount sample = UserAccountFactory.validNew();
        when(userAccountService.getByUsername(anyString())).thenReturn(sample);
        perform(put("/user/alan"), UserAccountFactory.toBase(sample)).
                andExpect(status().isOk()).
                andExpect(content().json(mapper.writeValueAsString(sample)));
    }

    @Test
    public void testUpdateBadPassword() throws Exception {
        when(userAccountService.getByUsername(anyString())).thenReturn(UserAccountFactory.valid());
        perform(put("/user/alan"), BaseUserAccount.builder().password("312").build()).
                andExpect(status().isBadRequest()).
                andExpect(content().string(
                        containsString("password: " + Messages.BAD_PASSWORD)
                ));
    }

    @Test
    public void testDelete() throws Exception {
        perform(delete("/user/alan"), null).
                andExpect(status().isNoContent());
    }

    @Test
    public void testIndex() throws Exception {
        Page<UserAccount> users = new PageImpl<>(Arrays.asList(
                UserAccountFactory.builder().username("user 1").build(),
                UserAccountFactory.builder().username("user 2").build()
        ));
        when(userAccountService.getAll(anyString(), any(Pageable.class))).thenReturn(users);
        perform(get("/user"), null).
                andExpect(status().isOk()).
                andExpect(content().json(mapper.writeValueAsString(users)));
    }

    @Test
    public void testShow() throws Exception {
        UserAccount sample = UserAccountFactory.valid();
        when(userAccountService.getByUsername(anyString())).thenReturn(sample);
        perform(get("/user/alan"), null).
                andExpect(status().isOk()).
                andExpect(content().json(mapper.writeValueAsString(sample)));
    }

}
