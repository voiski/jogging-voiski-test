package com.voiski.toptal.controller;

import com.fasterxml.jackson.annotation.JsonInclude;
import com.fasterxml.jackson.core.type.TypeReference;
import com.fasterxml.jackson.databind.ObjectMapper;
import com.voiski.toptal.exception.GlobalExceptionHandler;
import com.voiski.toptal.util.APIVersion;
import org.junit.BeforeClass;
import org.junit.runner.RunWith;
import org.mockito.Mock;
import org.mockito.MockitoAnnotations;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.context.support.StaticApplicationContext;
import org.springframework.data.web.PageableHandlerMethodArgumentResolver;
import org.springframework.http.MediaType;
import org.springframework.security.core.Authentication;
import org.springframework.security.core.context.SecurityContext;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.test.web.servlet.MockMvc;
import org.springframework.test.web.servlet.MvcResult;
import org.springframework.test.web.servlet.ResultActions;
import org.springframework.test.web.servlet.request.MockHttpServletRequestBuilder;
import org.springframework.test.web.servlet.setup.MockMvcBuilders;
import org.springframework.web.servlet.config.annotation.WebMvcConfigurationSupport;

import java.io.IOException;

import static org.mockito.Mockito.when;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/15/16.
 */
@RunWith(MockitoJUnitRunner.class)
public abstract class AbstractControllerTest {

    @Mock
    Authentication authentication;

    @Mock
    private SecurityContext securityContext;

    MockMvc mockMvc;
    static ObjectMapper mapper;

    @BeforeClass
    public static void initClass() {
        mapper = new ObjectMapper();
        mapper.findAndRegisterModules();
        mapper.setSerializationInclusion(JsonInclude.Include.NON_NULL);
    }

    void init(Object obj) {
        final StaticApplicationContext applicationContext = new StaticApplicationContext();
        applicationContext.registerSingleton("exceptionHandler", GlobalExceptionHandler.class);

        final WebMvcConfigurationSupport webMvcConfigurationSupport = new WebMvcConfigurationSupport();
        webMvcConfigurationSupport.setApplicationContext(applicationContext);

        MockitoAnnotations.initMocks(this);
        mockMvc = MockMvcBuilders.standaloneSetup(obj)
                .setHandlerExceptionResolvers(webMvcConfigurationSupport.handlerExceptionResolver())
                .setCustomArgumentResolvers(new PageableHandlerMethodArgumentResolver())
                .build();

        when(securityContext.getAuthentication()).thenReturn(authentication);
        SecurityContextHolder.setContext(securityContext);
    }

    ResultActions perform(MockHttpServletRequestBuilder endPoint, Object obj) throws Exception {
        endPoint.accept(APIVersion.V1);
        if (obj != null) {
            endPoint.content(mapper.writeValueAsString(obj)).contentType(MediaType.APPLICATION_JSON);
        }
        return mockMvc.perform(endPoint);
    }

    <T> T parseJson(MvcResult result, TypeReference<T> reference) throws IOException {
        return mapper.readValue(
                result.getResponse().getContentAsString(), reference
        );
    }

}
