package com.voiski.toptal.controller;

import com.voiski.toptal.entity.JogRecord;
import com.voiski.toptal.factory.JogRecordFactory;
import com.voiski.toptal.service.JogRecordService;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.Arrays;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.*;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/15/16.
 */
public class JogRecordControllerTest extends AbstractControllerTest {

    @InjectMocks
    private JogRecordController jogRecordController;

    @Mock
    private JogRecordService jogRecordService;

    @Before
    public void init() {
        init(jogRecordController);
    }

    @Test
    public void testCreate() throws Exception {
        JogRecord sample = JogRecordFactory.valid();
        perform(post("/user/alan/jog"), sample).
                andExpect(status().isCreated()).
                andExpect(content().json(mapper.writeValueAsString(sample)));
    }

    @Test
    public void testUpdate() throws Exception {
        JogRecord sample = JogRecordFactory.valid();
        when(jogRecordService.getByUsernameAndJogNumber(anyString(), any(Integer.class))).
                thenReturn(sample);
        perform(put("/user/alan/jog/1"), sample).
                andExpect(status().isOk()).
                andExpect(content().json(mapper.writeValueAsString(sample)));
    }

    @Test
    public void testDelete() throws Exception {
        perform(delete("/user/alan/jog/1"), null).
                andExpect(status().isNoContent());
    }

    @Test
    public void testIndex() throws Exception {
        val jogs = new PageImpl<JogRecord>(Arrays.asList(
                JogRecordFactory.builder().date(LocalDateTime.now().minusDays(3)).build(),
                JogRecordFactory.builder().date(LocalDateTime.now().minusDays(2)).build(),
                JogRecordFactory.builder().date(LocalDateTime.now().minusDays(1)).build()
        ));
        when(jogRecordService.getAllByUserAccount(anyString(), anyString(), any(Pageable.class))).thenReturn(jogs);
        perform(get("/user/alan/jog"), null).
                andExpect(status().isOk()).
                andExpect(content().json(mapper.writeValueAsString(jogs)));
    }

    @Test
    public void testShow() throws Exception {
        JogRecord sample = JogRecordFactory.valid();
        when(jogRecordService.getByUsernameAndJogNumber(anyString(), any(Integer.class))).
                thenReturn(sample);
        perform(get("/user/alan/jog/1"), null).
                andExpect(status().isOk()).
                andExpect(content().json(mapper.writeValueAsString(sample)));
    }

}
