package com.voiski.toptal.controller;

import com.voiski.toptal.entity.JogReport;
import com.voiski.toptal.factory.ReportFactory;
import com.voiski.toptal.service.ReportService;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;

import java.time.LocalDate;

import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.springframework.test.web.servlet.request.MockMvcRequestBuilders.get;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.content;
import static org.springframework.test.web.servlet.result.MockMvcResultMatchers.status;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/24/16.
 */
public class ReportControllerTest extends AbstractControllerTest {

    @InjectMocks
    private ReportController reportController;

    @Mock
    private ReportService reportService;

    @Before
    public void init() {
        init(reportController);
    }

    @Test
    public void testReportWeek() throws Exception {
        JogReport report = ReportFactory.valid();
        when(reportService.getAverageSpeedDistance(anyString(), any(LocalDate.class))).
                thenReturn(report);
        perform(get("/user/alan/report/week"), null).
                andExpect(status().isOk()).
                andExpect(content().json(mapper.writeValueAsString(report)));
    }

    @Test
    public void testReportWeekFromDate() throws Exception {
        JogReport report = ReportFactory.valid();
        when(reportService.getAverageSpeedDistance(anyString(), any(LocalDate.class))).
                thenReturn(report);
        perform(get("/user/alan/report/week?date=2016-12-24"), null).
                andExpect(status().isOk()).
                andExpect(content().json(mapper.writeValueAsString(report)));
    }

}