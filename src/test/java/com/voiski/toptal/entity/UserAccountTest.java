package com.voiski.toptal.entity;

import com.voiski.toptal.factory.UserAccountFactory;
import com.voiski.toptal.util.Messages;
import lombok.val;
import org.junit.Test;

import javax.validation.Validation;
import javax.validation.Validator;
import java.text.MessageFormat;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.*;
import static org.junit.Assert.assertThat;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/15/16.
 */
public class UserAccountTest {

    private Validator validator = Validation
            .buildDefaultValidatorFactory()
            .getValidator();

    @Test
    public void testSuccess() {
        val constraintViolations = validator.validate(UserAccountFactory.valid());
        assertThat(constraintViolations, empty());
    }

    @Test
    public void testEmptyFields() {
        val constraintViolations = validator.validate(UserAccount.builder().username("").build());
        assertThat(constraintViolations.size(), equalTo(4));
        Set<String> messages = constraintViolations.stream().map((constraint) ->
                MessageFormat.format("{0}: {1}", constraint.getPropertyPath(), constraint.getMessage()))
                .collect(Collectors.toSet());
        assertThat(messages, containsInAnyOrder("username: size must be between 3 and 30",
                                                "username: may not be empty",
                                                "password: may not be empty",
                                                "roles: may not be empty"));
    }

    @Test
    public void testBadPasswordMissingUppercaseLetter() {
        assertPassword("g00d_p4ssword");
    }

    @Test
    public void testBadPasswordMissingLowercaseLetter() {
        assertPassword("G00D_P4SSWORD");
    }

    @Test
    public void testBadPasswordMissingNumber() {
        assertPassword("good_Password");
    }

    @Test
    public void testBadPasswordMissingSpecialCharacter() {
        assertPassword("g00dP4ssword");
    }

    @Test
    public void testBadPasswordTooSmall() {
        assertPassword("g00d_P4");
    }

    private void assertPassword(String badPassword) {
        val constraintViolations = validator.validate(UserAccountFactory.builder().password(badPassword).build());
        assertThat(constraintViolations.size(), equalTo(1));
        constraintViolations.forEach((constraint) -> assertThat(constraint.getMessage(), equalTo(Messages.BAD_PASSWORD)));
    }
}
