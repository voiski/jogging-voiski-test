package com.voiski.toptal.entity;

import com.voiski.toptal.factory.JogRecordFactory;
import lombok.val;
import org.junit.Test;

import javax.validation.Validation;
import javax.validation.Validator;

import java.text.MessageFormat;
import java.util.Set;
import java.util.stream.Collectors;

import static org.hamcrest.Matchers.containsInAnyOrder;
import static org.hamcrest.Matchers.empty;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/15/16.
 */
public class JogRecordTest {

    private Validator validator = Validation
            .buildDefaultValidatorFactory()
            .getValidator();

    @Test
    public void testSuccess() {
        val constraintViolations = validator.validate(JogRecordFactory.valid());
        assertThat(constraintViolations, empty());
    }

    @Test
    public void testEmptyFields() {
        val constraintViolations = validator.validate(new JogRecord());
        assertThat(constraintViolations.size(), equalTo(6));
        Set<String> messages = constraintViolations.stream().map((constraint) ->
                MessageFormat.format("{0}: {1}", constraint.getPropertyPath(), constraint.getMessage()))
                .collect(Collectors.toSet());
        assertThat(messages, containsInAnyOrder("userAccount: may not be null",
                "jogNumber: may not be null",
                "date: may not be null",
                "distance: may not be null",
                "time: may not be null",
                "location: may not be empty"));
    }
}
