package com.voiski.toptal.client;

import com.voiski.toptal.client.wrap.WeatherWrap;
import com.voiski.toptal.entity.Weather;
import com.voiski.toptal.factory.WeatherWrapFactory;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.mockito.runners.MockitoJUnitRunner;
import org.springframework.web.client.RestTemplate;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.anyString;
import static org.mockito.Matchers.eq;
import static org.mockito.Mockito.when;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/16/16.
 */
@RunWith(MockitoJUnitRunner.class)
public class OpenWeatherClientTest {

    @InjectMocks
    private OpenWeatherClient client;

    @Mock
    private RestTemplate restTemplate;

    @Before
    public void setup(){
        client.setApiUrl("http://url?lat={}&lon={}&key={}");
    }

    @Test
    public void testWeather() {
        WeatherWrap wrap = WeatherWrapFactory.valid();
        when(restTemplate.getForObject(anyString(), eq(WeatherWrap.class))).thenReturn(wrap);
        Weather weather = client.getWeather(10,20);
        assertThat(weather.getClimate(), equalTo(wrap.getMain().getTemp()));
        assertThat(weather.getHumidity(), equalTo(wrap.getMain().getHumidity()));
        assertThat(weather.getWindSpeed(), equalTo(wrap.getWind().getSpeed()));
        assertThat(weather.getDescription(), equalTo(wrap.getDescription()));
    }
}
