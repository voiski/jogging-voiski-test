package com.voiski.toptal.client.wrap;

import org.junit.Test;

import java.util.Collections;

import static org.hamcrest.Matchers.equalTo;
import static org.hamcrest.Matchers.nullValue;
import static org.junit.Assert.assertThat;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/17/16.
 */
public class WeatherWrapTest {

    @Test
    public void testGetDescription(){
        WeatherDetailsWrap details = WeatherDetailsWrap.builder().description("Clear sky").build();
        WeatherWrap weatherWrap = WeatherWrap.builder().weather(Collections.singletonList(details)).build();
        assertThat(weatherWrap.getDescription(),equalTo(details.getDescription()));
    }

    @Test
    public void testGetDescriptionNull(){
        assertThat(new WeatherWrap().getDescription(),nullValue());
    }


}
