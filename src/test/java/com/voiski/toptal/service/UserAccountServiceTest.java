package com.voiski.toptal.service;

import com.querydsl.core.types.Predicate;
import com.voiski.toptal.entity.UserAccount;
import com.voiski.toptal.entity.UserRoles;
import com.voiski.toptal.exception.NoResultServiceException;
import com.voiski.toptal.factory.UserAccountFactory;
import com.voiski.toptal.repository.UserAccountRepository;
import com.voiski.toptal.repository.query.QueryBuilder;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.util.Arrays;

import static org.hamcrest.Matchers.contains;
import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/15/16.
 */
public class UserAccountServiceTest {

    @InjectMocks
    private UserAccountService userAccountService;

    @Mock
    private UserAccountRepository userAccountRepository;

    @Mock
    private QueryBuilder<UserAccount> queryBuilder;

    @Before
    public void init() {
        initMocks(this);
    }

    @Test
    public void testCreate() {
        UserAccount user = new UserAccount();
        when(userAccountRepository.save(any(UserAccount.class))).thenReturn(UserAccountFactory.valid());
        userAccountService.create(user);
        assertThat(user.getRoles().size(), equalTo(1));
        assertThat(user.getRoles(), contains(UserRoles.USER));
    }

    @Test
    public void testCreateWithRoles() {
        UserAccount user = UserAccountFactory.builder().
                id(null).
                roles(Arrays.asList(UserRoles.MANAGER, UserRoles.ADMIN)).
                build();
        when(userAccountRepository.save(any(UserAccount.class))).thenReturn(UserAccountFactory.valid());
        userAccountService.create(user);
        assertThat(user.getRoles().size(), equalTo(2));
        assertThat(user.getRoles(), contains(UserRoles.MANAGER, UserRoles.ADMIN));
    }

    @Test(expected = IllegalArgumentException.class)
    public void testCreateInvalidAlreadyAttached() {
        userAccountService.create(UserAccountFactory.valid());
    }

    @Test
    public void testUpdate() {
        when(userAccountRepository.save(any(UserAccount.class))).thenReturn(UserAccountFactory.valid());
        userAccountService.save(UserAccountFactory.valid());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testUpdateUnexistingUser() {
        userAccountService.save(new UserAccount());
    }

    @Test
    public void testDelete() {
        userAccountService.delete(UserAccountFactory.valid());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteUnexistingUser() {
        userAccountService.delete(new UserAccount());
    }

    @Test
    public void testGetByUsername() {
        UserAccount sample = UserAccountFactory.valid();
        when(userAccountRepository.findByUsername(any(String.class))).thenReturn(sample);
        UserAccount user = userAccountService.getByUsername("any");
        assertThat(user, equalTo(sample));
    }

    @Test(expected = NoResultServiceException.class)
    public void testGetByUsernameNotFound() {
        userAccountService.getByUsername("any");
    }

    @Test
    public void testGetAll() {
        val users = new PageImpl<UserAccount>(Arrays.asList(
                UserAccountFactory.builder().username("user 1").build(),
                UserAccountFactory.builder().username("user 2").build()
        ));
        when(userAccountRepository.findAll(any(Pageable.class))).thenReturn(users);
        val jogs = userAccountService.getAll(null, new PageRequest(1, 1));
        assertThat(jogs, equalTo(users));
    }

    @Test
    public void testGetAllWithQuery() {
        val users = new PageImpl<UserAccount>(Arrays.asList(
                UserAccountFactory.builder().username("user 1").build(),
                UserAccountFactory.builder().username("user 2").build()
        ));
        when(userAccountRepository.findAll(any(Predicate.class), any(Pageable.class))).thenReturn(users);
        val jogs = userAccountService.getAll("(x eq y)", new PageRequest(1, 1));
        assertThat(jogs, equalTo(users));
    }
}
