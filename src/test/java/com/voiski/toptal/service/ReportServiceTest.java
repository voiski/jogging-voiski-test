package com.voiski.toptal.service;

import com.voiski.toptal.entity.JogRecord;
import com.voiski.toptal.entity.JogReport;
import com.voiski.toptal.entity.UserAccount;
import com.voiski.toptal.factory.JogRecordFactory;
import com.voiski.toptal.util.DateUtil;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.beans.factory.annotation.Autowired;

import java.time.LocalDate;
import java.time.LocalDateTime;
import java.util.Arrays;
import java.util.concurrent.TimeUnit;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/24/16.
 */
public class ReportServiceTest {

    @InjectMocks
    private ReportService jogReportService;

    @Mock
    private UserAccountService userAccountService;

    @Mock
    private JogRecordService jogRecordService;

    @Autowired
    private JogRecordFactory jogRecordFactory;

    @Before
    public void init() {
        initMocks(this);
    }

    @Test
    public void testGetAverageSpeedDistance() throws Exception {
        UserAccount userAccount = new UserAccount();
        JogRecord jogRecord = jogRecordFactory.builder().distance(5000).time(
                TimeUnit.MINUTES.toSeconds(25)
        ).build();

        when(userAccountService.getByUsername(anyString())).thenReturn(userAccount);
        when(jogRecordService.getByRange(anyString(), any(LocalDateTime.class), any(LocalDateTime.class))).
                thenReturn(Arrays.asList(
                        jogRecord,
                        jogRecord,
                        jogRecord,
                        jogRecord,
                        jogRecord
                ));
        LocalDate referenceDate = LocalDate.of(2016, 1, 1);
        JogReport jogReport = jogReportService.getAverageSpeedDistance("username", referenceDate);
        assertThat(jogReport.getWeek(), equalTo(1));
        assertThat(jogReport.getFromDate(), equalTo(DateUtil.getFirstDayOfTheWeek(referenceDate)));
        assertThat(jogReport.getToDate(), equalTo(DateUtil.getLastDayOfTheWeek(referenceDate)));
        assertThat(jogReport.getTotalDistance(), equalTo(25D));
        assertThat(jogReport.getAverageSpeed(), equalTo(12d));
    }

}