package com.voiski.toptal.service;

import com.querydsl.core.types.Predicate;
import com.voiski.toptal.client.OpenWeatherClient;
import com.voiski.toptal.entity.JogRecord;
import com.voiski.toptal.entity.UserAccount;
import com.voiski.toptal.exception.NoResultServiceException;
import com.voiski.toptal.factory.JogRecordFactory;
import com.voiski.toptal.repository.CounterRepository;
import com.voiski.toptal.repository.JogRecordRepository;
import com.voiski.toptal.repository.query.QueryBuilder;
import lombok.val;
import org.junit.Before;
import org.junit.Test;
import org.mockito.InjectMocks;
import org.mockito.Mock;
import org.springframework.data.domain.PageImpl;
import org.springframework.data.domain.PageRequest;
import org.springframework.data.domain.Pageable;

import java.time.LocalDateTime;
import java.util.Collections;

import static org.hamcrest.Matchers.equalTo;
import static org.junit.Assert.assertThat;
import static org.mockito.Matchers.any;
import static org.mockito.Matchers.anyString;
import static org.mockito.Mockito.when;
import static org.mockito.MockitoAnnotations.initMocks;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/15/16.
 */
public class JogRecordServiceTest {

    @InjectMocks
    private JogRecordService jogRecordService;

    @Mock
    private UserAccountService userAccountService;

    @Mock
    private JogRecordRepository jogRecordRepository;

    @Mock
    private CounterRepository counterRepository;

    @Mock
    private OpenWeatherClient openWeatherClient;

    @Mock
    private QueryBuilder<JogRecord> queryBuilder;

    @Before
    public void init() {
        initMocks(this);
    }

    @Test
    public void testCreate() {
        when(userAccountService.getByUsername(anyString())).thenReturn(new UserAccount());
        when(jogRecordRepository.save(any(JogRecord.class))).thenReturn(JogRecordFactory.valid());
        jogRecordService.create("anyUser", new JogRecord());
    }

    @Test
    public void testUpdate() {
        when(userAccountService.getByUsername(anyString())).thenReturn(new UserAccount());
        when(jogRecordRepository.save(any(JogRecord.class))).thenReturn(JogRecordFactory.valid());
        jogRecordService.save("anyUser", JogRecordFactory.valid());
    }

    @Test
    public void testDelete() {
        jogRecordService.delete(JogRecordFactory.valid());
    }

    @Test(expected = IllegalArgumentException.class)
    public void testDeleteUnexistingRecord() {
        jogRecordService.delete(new JogRecord());
    }

    @Test
    public void testGetAllByUserAccount() {
        JogRecord sample = JogRecordFactory.valid();
        when(userAccountService.getByUsername(anyString())).thenReturn(new UserAccount());
        when(jogRecordRepository.
                findByUserAccount(any(UserAccount.class), any(Pageable.class))).
                thenReturn(new PageImpl<JogRecord>(Collections.singletonList(sample)));
        val jogs = jogRecordService.getAllByUserAccount("any", null, new PageRequest(1, 1));
        assertThat(jogs.iterator().next(), equalTo(sample));
    }

    @Test
    public void testGetAllByUserAccountWithQuery() {
        JogRecord sample = JogRecordFactory.valid();
        when(userAccountService.getByUsername(anyString())).thenReturn(new UserAccount());
        when(jogRecordRepository.
                findAll(any(Predicate.class), any(Pageable.class))).
                thenReturn(new PageImpl<JogRecord>(Collections.singletonList(sample)));
        val jogs = jogRecordService.
                getAllByUserAccount("any", "(x eq y)", new PageRequest(1, 1));
        assertThat(jogs.iterator().next(), equalTo(sample));
    }

    @Test
    public void testGetByRange() {
        JogRecord sample = JogRecordFactory.valid();
        when(userAccountService.getByUsername(anyString())).thenReturn(new UserAccount());
        when(jogRecordRepository.findAll(any(Predicate.class))).
                thenReturn(Collections.singletonList(sample));
        val jogs = jogRecordService.
                getByRange("any", LocalDateTime.now(), LocalDateTime.now());
        assertThat(jogs.iterator().next(), equalTo(sample));
    }

    @Test
    public void testGetByUsernameAndJogNumber() {
        JogRecord sample = JogRecordFactory.valid();
        when(userAccountService.getByUsername(anyString())).thenReturn(new UserAccount());
        when(jogRecordRepository.
                findByUserAccountAndJogNumber(any(UserAccount.class), any(Integer.class))).
                thenReturn(sample);
        JogRecord jog = jogRecordService.getByUsernameAndJogNumber("any", sample.getJogNumber());
        assertThat(jog, equalTo(sample));
    }

    @Test(expected = NoResultServiceException.class)
    public void testGetByUsernameAndJogNumberNotFound() {
        when(userAccountService.getByUsername(anyString())).thenReturn(new UserAccount());
        jogRecordService.getByUsernameAndJogNumber("any", 1);
    }
}
