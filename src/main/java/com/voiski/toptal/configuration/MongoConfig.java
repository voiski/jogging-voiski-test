package com.voiski.toptal.configuration;

import com.mongodb.Mongo;
import com.mongodb.MongoClient;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.mongodb.MongoDbFactory;
import org.springframework.data.mongodb.config.AbstractMongoConfiguration;
import org.springframework.data.mongodb.core.convert.DefaultDbRefResolver;
import org.springframework.data.mongodb.core.convert.MappingMongoConverter;
import org.springframework.data.mongodb.core.mapping.MongoMappingContext;
import org.springframework.data.mongodb.repository.config.EnableMongoRepositories;

/**
 * Mongo access custom configurations
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 */
@Configuration
@EnableMongoRepositories(basePackages = "com.voiski.toptal.repository")
public class MongoConfig extends AbstractMongoConfiguration {

    @Value("${DB_HOST:127.0.0.1:27017}")
    private String dbHost;

    @Autowired
    MongoDbFactory mongoDbFactory;

    @Override
    protected String getDatabaseName() {
        return "toptal-voiski-test";
    }

    @Override
    public Mongo mongo() throws Exception {
        String[] dbHostPort = dbHost.split(":");
        return new MongoClient(dbHostPort[0], Integer.valueOf(dbHostPort[1]));
    }

    @Bean
    public MappingMongoConverter getDefaultMongoConverter() throws Exception {

        MappingMongoConverter converter = new MappingMongoConverter(
                new DefaultDbRefResolver(mongoDbFactory), new MongoMappingContext());

        return converter;
    }
}
