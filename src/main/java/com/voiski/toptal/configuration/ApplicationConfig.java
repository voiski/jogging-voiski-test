package com.voiski.toptal.configuration;

import com.fasterxml.jackson.databind.ObjectMapper;
import com.voiski.toptal.client.OpenWeatherClient;
import com.voiski.toptal.entity.JogRecord;
import com.voiski.toptal.entity.QJogRecord;
import com.voiski.toptal.entity.QUserAccount;
import com.voiski.toptal.entity.UserAccount;
import com.voiski.toptal.repository.query.QueryBuilder;
import org.springframework.context.annotation.Bean;
import org.springframework.context.annotation.Configuration;
import org.springframework.data.web.config.EnableSpringDataWebSupport;
import org.springframework.web.client.RestTemplate;

/**
 * General configuration with bean declarations
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/16/16.
 */
@Configuration
@EnableSpringDataWebSupport
public class ApplicationConfig {

    @Bean(name = "appObjectMapper")
    public ObjectMapper getObjectMapper() {
        ObjectMapper objectMapper = new ObjectMapper();
        objectMapper.findAndRegisterModules();
        return objectMapper;
    }

    @Bean
    public OpenWeatherClient getOpenWeatherClient(){
        return new OpenWeatherClient();
    }

    @Bean
    public RestTemplate getRestTemplate(){
        return new RestTemplate();
    }

    @Bean
    public QueryBuilder<UserAccount> getQueryBuilderUserAccount() {
        return new QueryBuilder(QUserAccount.userAccount);
    }

    @Bean
    public QueryBuilder<JogRecord> getQueryBuilderJogRecord() {
        return new QueryBuilder(QJogRecord.jogRecord);
    }
}
