package com.voiski.toptal.configuration;

import com.voiski.toptal.entity.UserRoles;
import com.voiski.toptal.service.UserAuthentication;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.context.annotation.Configuration;
import org.springframework.http.HttpMethod;
import org.springframework.security.config.annotation.authentication.builders.AuthenticationManagerBuilder;
import org.springframework.security.config.annotation.web.builders.HttpSecurity;
import org.springframework.security.config.annotation.web.configuration.EnableWebSecurity;
import org.springframework.security.config.annotation.web.configuration.WebSecurityConfigurerAdapter;

/**
 * Security configuration to use the user account service as the authentication
 * entity.
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 */
@Configuration
@EnableWebSecurity
public class SecurityConfig extends WebSecurityConfigurerAdapter {

    @Autowired
    UserAuthentication userDetailsService;

    @Autowired
    public void configAuthBuilder(AuthenticationManagerBuilder builder) throws Exception {
        builder.userDetailsService(userDetailsService);
    }

    @Override
    protected void configure(HttpSecurity http) throws Exception {
        // TODO: change to keep it
        http.csrf().disable();
        http.authorizeRequests()
                // To create a user you can skip the login
                .antMatchers(HttpMethod.POST, "/user").permitAll()
                // User can change itself, manager and admin can change any
                .antMatchers("/user/{username}").access(
                        "isAuthenticated() and principal.username==#username or isAuthenticated() and !hasRole('USER')"
                )
                // User can't search others, manager and admin can
                .antMatchers(HttpMethod.GET, "/user").hasAnyRole(
                        UserRoles.MANAGER, UserRoles.ADMIN
                )
                // User can manage their jogs, admin can change any
                .antMatchers("/user/{username}/jog/**").access(
                        "isAuthenticated() and principal.username==#username or hasRole('ADMIN')"
                )
                // User can see the own reports, manager and admin can see from anyone
                .antMatchers("/user/{username}/report/**").access(
                        "isAuthenticated() and principal.username==#username or isAuthenticated() and !hasRole('USER')"
                )

                .and().httpBasic();
    }

}
