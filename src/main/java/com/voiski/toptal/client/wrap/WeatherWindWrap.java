package com.voiski.toptal.client.wrap;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Wrap class for the wind information. We only need the speed, but the payload will have also the degree.
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/16/16.
 * @see <a href="https://openweathermap.org/current#geo">https://openweathermap.org/current#geo</a>
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WeatherWindWrap {
    private Double speed;
}
