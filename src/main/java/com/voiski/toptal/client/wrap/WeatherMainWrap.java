package com.voiski.toptal.client.wrap;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

/**
 * Wrap class for the main weather information containing information about the temperature, pressure and other
 * attributes. We only need some attributes, but the payload will have more.
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/16/16.
 * @see <a href="https://openweathermap.org/current#geo">https://openweathermap.org/current#geo</a>
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WeatherMainWrap {
    private Double temp;
    private Integer humidity;
}
