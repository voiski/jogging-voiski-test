package com.voiski.toptal.client.wrap;

import com.voiski.toptal.entity.Weather;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Getter;
import lombok.NoArgsConstructor;

import java.util.Collection;

/**
 * Wrap class to the payload returned from the OpenWeather API client. The API return more information, this wrap
 * contains only the necessary fields for the jog information.
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/16/16.
 * @see <a href="https://openweathermap.org/current#geo">https://openweathermap.org/current#geo</a>
 */
@Getter
@Builder
@NoArgsConstructor
@AllArgsConstructor
public class WeatherWrap {
    private Collection<WeatherDetailsWrap> weather;
    private WeatherMainWrap main;
    private WeatherWindWrap wind;

    /**
     * Retrieves the weather description from the weather list properties.
     *
     * @return weather description
     * @see WeatherDetailsWrap
     */
    public String getDescription() {
        return weather != null && !weather.isEmpty() ?
                weather.iterator().next().getDescription() : null;
    }

    /**
     * Maps each properties returned from the OpenWeather to the jog weather entity
     *
     * @return weather entity filled with the wrap information
     */
    public Weather transform() {
        return Weather.builder().
                description(getDescription()).
                climate(main.getTemp()).
                humidity(main.getHumidity()).
                windSpeed(wind.getSpeed()).
                build();
    }
}
