package com.voiski.toptal.client;

import com.voiski.toptal.client.wrap.WeatherWrap;
import com.voiski.toptal.entity.Weather;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.beans.factory.annotation.Value;
import org.springframework.web.client.RestTemplate;

/**
 * OpenWeather API client to retrive the current weather by the location.
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @see <a href="https://openweathermap.org/current#geo">https://openweathermap.org/current#geo</a>
 * @since 12/16/16
 */
@Slf4j
public class OpenWeatherClient {

    @Autowired
    private RestTemplate restTemplate;

    /**
     * API key to have valid access to the OpenWeather service API
     */
    @Value("${openweather.key}")
    private String apiKey;

    /**
     * API endpoint url template to get by geolocation
     */
    @Value("${openweather.url}")
    private String apiUrl;


    /**
     * Retrieves the weather information based on the get location.
     *
     * @param lat latitude
     * @param lon longitude
     * @return weather entity filled with the service response; null if the location is invalid or not available
     */
    public Weather getWeather(double lat, double lon) {
        log.debug("Retrieving current weather by location lat {}, lon {}", lat, lon);
        String endpoint = String.format(apiUrl, lat, lon, apiKey);
        WeatherWrap response = restTemplate.getForObject(endpoint, WeatherWrap.class);
        if (response != null) {
            log.debug("OpenWeather response: {}", response);
            return response.transform();
        } else {
            log.debug("OpenWeather was unable to retrieves weather information.");
            return null;
        }
    }

    /**
     * This is more for test purpose
     *
     * @param apiUrl url api
     */
    void setApiUrl(String apiUrl) {
        this.apiUrl = apiUrl;
    }
}
