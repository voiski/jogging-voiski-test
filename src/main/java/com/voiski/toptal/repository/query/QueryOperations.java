package com.voiski.toptal.repository.query;

import com.querydsl.core.types.dsl.*;
import com.voiski.toptal.util.DateUtil;

import java.time.LocalDateTime;
import java.util.Collections;
import java.util.HashMap;
import java.util.Map;
import java.util.function.BiFunction;


/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/18/16.
 */
public class QueryOperations {
    public static final Map<String, BiFunction<SimpleExpression, Object, BooleanExpression>> OPERATIONS;

    static {
        Map<String, BiFunction<SimpleExpression, Object, BooleanExpression>> validOperations = new HashMap<>();
        validOperations.put("eq", QueryOperations::eq);
        validOperations.put("ne", QueryOperations::ne);
        validOperations.put("gt", QueryOperations::gt);
        validOperations.put("goe", QueryOperations::goe);
        validOperations.put("lt", QueryOperations::lt);
        validOperations.put("loe", QueryOperations::loe);
        OPERATIONS = Collections.unmodifiableMap(validOperations);
    }

    public static BooleanExpression eq(SimpleExpression path, Object value) {
        BooleanExpression criteria;
        if (path instanceof DateTimePath &&
                value.toString().length() < DateUtil.PATTERN_DATETIME.length()) {
            criteria = eqDateInterval((DateTimePath) path, DateUtil.parse(value.toString()));
        } else {
            criteria = path.eq(convert(path, value));
        }
        return criteria;
    }

    private static BooleanExpression eqDateInterval(DateTimePath path, LocalDateTime value) {
        LocalDateTime plusDateValue;
        if (value.getMinute() > 0) {
            plusDateValue = value.plusMinutes(1);
        } else if (value.getHour() > 0) {
            plusDateValue = value.plusHours(1);
        } else {
            plusDateValue = value.plusDays(1);
        }
        return path.goe(value).and(path.lt(plusDateValue));
    }

    public static BooleanExpression ne(SimpleExpression path, Object value) {
        return path.ne(convert(path, value));
    }

    public static BooleanExpression gt(SimpleExpression path, Object value) {
        BooleanExpression criteria;
        if (path instanceof LiteralExpression) {
            criteria = ((LiteralExpression) path).gt((Comparable) convert(path, value));
        } else {
            criteria = ((NumberExpression) path).gt(toNumber(path, value));
        }
        return criteria;
    }

    public static BooleanExpression goe(SimpleExpression path, Object value) {
        BooleanExpression criteria;
        if (path instanceof LiteralExpression) {
            criteria = ((LiteralExpression) path).goe((Comparable) convert(path, value));
        } else {
            criteria = ((NumberExpression) path).goe(toNumber(path, value));
        }
        return criteria;
    }

    public static BooleanExpression lt(SimpleExpression path, Object value) {
        BooleanExpression criteria;
        if (path instanceof LiteralExpression) {
            criteria = ((LiteralExpression) path).lt((Comparable) convert(path, value));
        } else {
            criteria = ((NumberExpression) path).lt(toNumber(path, value));
        }
        return criteria;
    }

    public static BooleanExpression loe(SimpleExpression path, Object value) {
        BooleanExpression criteria;
        if (path instanceof LiteralExpression) {
            criteria = ((LiteralExpression) path).loe((Comparable) convert(path, value));
        } else {
            criteria = ((NumberExpression) path).loe(toNumber(path, value));
        }
        return criteria;
    }

    private static Object convert(SimpleExpression path, Object value) {
        Object convertedObject;
        if (path instanceof NumberExpression) {
            convertedObject = toNumber(path, value);
        } else if (path instanceof DateTimePath) {
            convertedObject = DateUtil.parse(value.toString());
        } else {
            convertedObject = value;
        }
        return convertedObject;
    }

    private static Number toNumber(SimpleExpression path, Object value) {
        String stringValue = value.toString();
        Number convertedValue;
        if (Double.class.isAssignableFrom(path.getType())) {
            convertedValue = Double.valueOf(stringValue);
        } else if (Long.class.isAssignableFrom(path.getType())) {
            convertedValue = Long.valueOf(stringValue);
        } else {
            convertedValue = Integer.valueOf(stringValue);
        }
        return convertedValue;
    }
}
