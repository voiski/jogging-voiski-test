package com.voiski.toptal.repository.query;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.querydsl.core.types.dsl.SimpleExpression;
import com.voiski.toptal.exception.InvalidQueryException;
import lombok.extern.slf4j.Slf4j;
import org.apache.commons.lang3.reflect.FieldUtils;

import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Predicate generator
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/18/16.
 */
@Slf4j
public class QueryPredicate<T> {
    /**
     * Pattern to get the groups, ex: G3=((G1)(G2))
     */
    public static final Pattern PATTERN_CRITERIA = Pattern.compile("\\(?(\\w+) (\\w{2,3}) '?([^()']*)'?\\)?");

    private EntityPathBase<T> base;

    public QueryPredicate(EntityPathBase<T> base) {
        this.base = base;
    }

    /**
     * Create a predicate boolean expression based on the field and the valid operations.
     *
     * @param key       field name
     * @param operation operation [eq|ne|gt|lt|goe|loe]
     * @param value     value to be compared
     * @return predicate for the given field
     * @throws InvalidQueryException if the field or the operation are wrong
     * @see QueryOperations#OPERATIONS
     */
    public BooleanExpression getPredicate(String key, String operation, Object value) {
        log.debug("Building predicate for {} field that should be {} '{}'", key, operation, value);

        BooleanExpression criteria = null;
        try {
            SimpleExpression path = (SimpleExpression) FieldUtils.readField(base, key);
            if (!QueryOperations.OPERATIONS.containsKey(operation)) {
                throw new InvalidQueryException("Invalid operation, use one of the options [eq|ne|gt|lt|goe|loe]");
            }
            criteria = QueryOperations.OPERATIONS.get(operation).apply(path, value);
        } catch (IllegalAccessException|IllegalArgumentException e) {
            throw new InvalidQueryException(e.getMessage());
        }
        return criteria;
    }

    /**
     * Create a predicate boolean expression based on a query dsl.
     *
     * @param criteria string criteria in the format (field [eq|ne|gt|lt|goe|loe] 'value')
     * @return predicate for the given field
     * @throws InvalidQueryException if the given criteria is invalid
     * @see #getPredicate(String, String, Object)
     */
    public BooleanExpression processCriteria(String criteria) {
        log.debug("Processing the criteria '{} to build the predicate", criteria);
        Matcher captured = PATTERN_CRITERIA.matcher(criteria);
        if (!captured.find()) {
            throw new InvalidQueryException("Bad criteria, use (field [eq|ne|gt|lt|goe|loe] 'value')");
        }
        return getPredicate(captured.group(1), captured.group(2), captured.group(3));
    }
}
