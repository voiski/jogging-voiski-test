package com.voiski.toptal.repository.query;

import com.querydsl.core.types.dsl.BooleanExpression;
import com.querydsl.core.types.dsl.EntityPathBase;
import com.voiski.toptal.exception.InvalidQueryException;
import lombok.extern.slf4j.Slf4j;

import java.util.HashMap;
import java.util.Map;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Query grouping builder, it will process the query dsl created for this project to handle grouping and logic operation.
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/18/16.
 */
@Slf4j
public class QueryBuilder<T> {

    /**
     * Pattern to get the groups, ex: G3=((G1)(G2))
     */
    public static final Pattern PATTERN_GROUP = Pattern.compile("\\([^()]+\\)");
    /**
     * Pattern to get the logic operation and the two critearias to be evaluated.<br/>
     * Ex:
     * <ul>
     * <li>($G1 <b>OR</b> $G2)</li>
     * <li>$G1 <b>AND</b> $G2</li>
     * </ul>
     */
    public static final Pattern PATTERN_LOGIC_OPERATION = Pattern.compile("\\(?(\\$G\\d+) (\\w{2,3}) (\\$G\\d+)\\)?");

    private Map<String, BooleanExpression> groups = new HashMap<>();

    private QueryPredicate<T> predicate;

    public QueryBuilder(EntityPathBase<T> base) {
        predicate = new QueryPredicate(base);
    }

    public static <T> QueryBuilder<T> create(EntityPathBase<T> base) {
        return new QueryBuilder<>(base);
    }

    /**
     * Process the given query identifying the groups and process each one to then apply the logical operation.
     *
     * @param requestedQuery query dsl
     * @return predicate expression to by applied as a filter on a search action
     */
    public BooleanExpression build(String requestedQuery) {
        log.debug("Initial query {'}", requestedQuery);

        int count = 0;

        StringBuilder query = new StringBuilder(requestedQuery);
        while (query.indexOf("(") != -1) {
            Matcher captured = PATTERN_GROUP.matcher(query.toString());
            while (captured.find()) {
                String group = captured.group();
                String groupId = "$G" + count++;
                int index = query.indexOf(group);
                query.replace(index, index + group.length(), groupId);
                log.debug("Found group '{}', replacing query with group key {}", group, groupId);
                if (group.contains("$G")) {
                    groups.put(groupId, processGroup(group));
                } else {
                    groups.put(groupId, predicate.processCriteria(group));
                }
            }
            log.debug("Current query situation {}", query);
        }
        if (count == 1) {
            return predicate.processCriteria(requestedQuery);
        } else {
            return processGroup(query.toString());
        }
    }

    private BooleanExpression processGroup(String group) {
        log.debug("Processing group {}", group);
        Matcher captured = PATTERN_LOGIC_OPERATION.matcher(group);
        if (!captured.find()) {
            throw new InvalidQueryException("Bad grouping, use (criteria 1) [OR|AND] (criteria 2)");
        }
        BooleanExpression one = groups.get(captured.group(1));
        String logicOperation = captured.group(2);
        BooleanExpression two = groups.get(captured.group(3));
        log.debug("Joining group {}, using logic operator {}, with {}", one, logicOperation, two);
        if ("or".equalsIgnoreCase(logicOperation)) {
            return one.or(two);
        } else {
            return one.and(two);
        }
    }


}
