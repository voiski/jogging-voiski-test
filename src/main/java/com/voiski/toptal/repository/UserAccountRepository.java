package com.voiski.toptal.repository;

import com.voiski.toptal.entity.UserAccount;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * User repository
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 */
@Repository
public interface UserAccountRepository extends PagingAndSortingRepository<UserAccount, String>, QueryDslPredicateExecutor<UserAccount> {

    /**
     * Get the user account by the username
     *
     * @param username username to the user reference
     * @return user account from the given username
     */
    UserAccount findByUsername(String username);

}
