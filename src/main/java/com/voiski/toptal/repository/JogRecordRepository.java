package com.voiski.toptal.repository;

import com.querydsl.core.types.Predicate;
import com.voiski.toptal.entity.JogRecord;
import com.voiski.toptal.entity.UserAccount;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.data.querydsl.QueryDslPredicateExecutor;
import org.springframework.data.repository.PagingAndSortingRepository;
import org.springframework.stereotype.Repository;

/**
 * Jog repository
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/15/16.
 */
@Repository
public interface JogRecordRepository extends PagingAndSortingRepository<JogRecord, String>, QueryDslPredicateExecutor<JogRecord> {

    /**
     * Search user jogs applying pagination.
     *
     * @param userAccount reference to filter jobs by user
     * @param pageable pagination feature
     * @return page object with list result
     */
    Page<JogRecord> findByUserAccount(UserAccount userAccount, Pageable pageable);

    /**
     * Search user jogs applying custom filter.
     *
     * @param userAccount reference to filter jobs by user
     * @param predicate query feature for custom filter
     * @return list result
     */
    @Deprecated
    Iterable<JogRecord> findByUserAccount(UserAccount userAccount, Predicate predicate);

    /**
     * Get jog record by user and sequence jog number
     *
     * @param userAccount reference to filter jobs by user
     * @param jogNumber sequence jog number
     * @return unique jog record
     */
    JogRecord findByUserAccountAndJogNumber(UserAccount userAccount, Integer jogNumber);
}
