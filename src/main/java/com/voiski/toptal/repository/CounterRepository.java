package com.voiski.toptal.repository;

import com.voiski.toptal.entity.Counter;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoOperations;
import org.springframework.data.mongodb.core.query.Update;
import org.springframework.stereotype.Repository;

import static org.springframework.data.mongodb.core.FindAndModifyOptions.options;
import static org.springframework.data.mongodb.core.query.Criteria.where;
import static org.springframework.data.mongodb.core.query.Query.query;

/**
 * Sequence generator
 *
 * @see <a href="http://stackoverflow.com/a/36469232/2985644">http://stackoverflow.com/a/36469232/2985644</a>
 */
@Slf4j
@Repository
public class CounterRepository {

    @Autowired
    private MongoOperations mongo;

    public int findNextSequence(String username) {
        Counter counter = mongo.findAndModify(
                query(where("_id").is(username)),
                new Update().inc("seq", 1),
                options().returnNew(true),
                Counter.class);
        if (counter == null) {
            counter = new Counter();
            counter.setId(username);
            counter.setSeq(1);
            mongo.save(counter);
        }
        log.debug("Sequence counter {}", counter);
        return counter.getSeq();
    }
}
