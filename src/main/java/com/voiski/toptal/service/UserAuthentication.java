package com.voiski.toptal.service;

import com.voiski.toptal.entity.UserAccount;
import com.voiski.toptal.repository.UserAccountRepository;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.security.core.authority.SimpleGrantedAuthority;
import org.springframework.security.core.userdetails.User;
import org.springframework.security.core.userdetails.UserDetails;
import org.springframework.security.core.userdetails.UserDetailsService;
import org.springframework.security.core.userdetails.UsernameNotFoundException;
import org.springframework.stereotype.Service;

import java.util.Collection;
import java.util.stream.Collectors;

/**
 * Authentication user details provider that override the load user details to use the user repository to retrieve the
 * user.
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 */
@Slf4j
@Service
public class UserAuthentication implements UserDetailsService {

    @Autowired
    private UserAccountRepository userRepository;

    @Override
    public UserDetails loadUserByUsername(String username) throws UsernameNotFoundException {
        log.debug("Trying to log with user {}", username);
        UserAccount user = userRepository.findByUsername(username);
        if (user == null) {
            throw new UsernameNotFoundException(username);
        } else {
            log.debug("Found user with id {} and roles {}", user.getId(), user.getRoles());
            Collection<SimpleGrantedAuthority> authorities = user.
                    getRoles().stream().
                    map((role) -> new SimpleGrantedAuthority("ROLE_".concat(role))).
                    collect(Collectors.toSet());
            return new User(username, user.getPassword(), authorities);
        }
    }
}
