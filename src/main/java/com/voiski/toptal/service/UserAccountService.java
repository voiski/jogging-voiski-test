package com.voiski.toptal.service;

import com.voiski.toptal.entity.UserAccount;
import com.voiski.toptal.entity.UserRoles;
import com.voiski.toptal.exception.NoResultServiceException;
import com.voiski.toptal.repository.UserAccountRepository;
import com.voiski.toptal.repository.query.QueryBuilder;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.CollectionUtils;
import org.springframework.util.StringUtils;

import java.util.Collections;

/**
 * User CRUD Service
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 */
@Service
public class UserAccountService {

    @Autowired
    private UserAccountRepository userRepository;

    @Autowired
    private QueryBuilder<UserAccount> queryBuilder;

    /**
     * Transactional service method to persist a new user account. If not role is given it will set the USER role.
     *
     * @param user user account object to be persisted
     * @see UserRoles#USER
     */
    @Transactional
    public void create(UserAccount user) {
        Assert.isNull(user.getId());
        if (CollectionUtils.isEmpty(user.getRoles())) {
            user.setRoles(Collections.singletonList(UserRoles.USER));
        }
        userRepository.save(user);
    }

    /**
     * Transactional service method to update a existing user account.
     *
     * @param user user account object to be persisted
     */
    @Transactional
    public void save(UserAccount user) {
        Assert.notNull(user.getId());
        userRepository.save(user);
    }

    /**
     * Transactional service method to delete a existing user account.
     *
     * @param user user account object to be deleted
     */
    @Transactional
    public void delete(UserAccount user) {
        Assert.notNull(user.getId());
        userRepository.delete(user);
    }

    /**
     * Search users using pagination and query function to filter.
     *
     * @param query    custom filter
     * @param pageable requested page definition
     * @return page object with list result
     */
    public Page<UserAccount> getAll(String query, Pageable pageable) {
        Page<UserAccount> page;
        if (!StringUtils.isEmpty(query)) {
            page = userRepository.findAll(queryBuilder.build(query), pageable);
        } else {
            page = userRepository.findAll(pageable);
        }
        return page;
    }

    /**
     * Retrieves the user account by the natural identify user username.
     *
     * @param username reference user username
     * @return unique user account
     */
    public UserAccount getByUsername(String username) {
        UserAccount userAccount = userRepository.findByUsername(username);
        if (userAccount == null) {
            throw new NoResultServiceException("Invalid user {0}", username);
        }
        return userAccount;
    }
}
