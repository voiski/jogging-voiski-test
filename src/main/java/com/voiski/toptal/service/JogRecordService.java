package com.voiski.toptal.service;

import com.querydsl.core.types.Predicate;
import com.voiski.toptal.client.OpenWeatherClient;
import com.voiski.toptal.entity.JogRecord;
import com.voiski.toptal.entity.QJogRecord;
import com.voiski.toptal.entity.Weather;
import com.voiski.toptal.exception.NoResultServiceException;
import com.voiski.toptal.repository.CounterRepository;
import com.voiski.toptal.repository.JogRecordRepository;
import com.voiski.toptal.repository.query.QueryBuilder;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.stereotype.Service;
import org.springframework.transaction.annotation.Transactional;
import org.springframework.util.Assert;
import org.springframework.util.StringUtils;

import java.time.LocalDateTime;

/**
 * CRUD Jog Record service
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/15/16.
 */
@Slf4j
@Service
public class JogRecordService {

    @Autowired
    private JogRecordRepository jogRecordRepository;

    @Autowired
    private CounterRepository counterRepository;

    @Autowired
    private QueryBuilder<JogRecord> queryBuilder;

    @Autowired
    private UserAccountService userAccountService;

    @Autowired
    private OpenWeatherClient openWeatherClient;

    /**
     * Transactional service method to persist a new jog record. It will retrieves the user account by the given
     * username, the current weather conditions and the next sequence jog number.
     *
     * @param username  user username
     * @param jogRecord jog record object to be persisted
     */
    @Transactional
    public void create(String username, JogRecord jogRecord) {
        Assert.isNull(jogRecord.getId());
        jogRecord.setUserAccount(userAccountService.getByUsername(username));
        jogRecord.setJogNumber(counterRepository.findNextSequence(username));
        jogRecord.setWeather(getWeather(jogRecord));

        log.debug("Adding new jog record ({}) to user: {}", jogRecord.getJogNumber(), username);
        jogRecordRepository.save(jogRecord);
    }

    /**
     * Transactional service method to update a existing jog record. It will retrieves the user account by the given
     * username.
     *
     * @param username  user username
     * @param jogRecord jog record object to be persisted
     */
    @Transactional
    public void save(String username, JogRecord jogRecord) {
        Assert.notNull(jogRecord.getId());
        jogRecord.setUserAccount(userAccountService.getByUsername(username));

        log.debug("Updating jog record {} from user: {}", jogRecord.getJogNumber(), username);
        jogRecordRepository.save(jogRecord);
    }

    /**
     * Transactional service method to delete a existing jog record.
     *
     * @param jogRecord jog record object to be deleted
     */
    @Transactional
    public void delete(JogRecord jogRecord) {
        Assert.notNull(jogRecord.getId());

        log.debug("Deleting jog record {} with id {}", jogRecord.getJogNumber(), jogRecord.getId());
        jogRecordRepository.delete(jogRecord);
    }

    /**
     * Retrieves the jog record by the natural identifies user username and jog number.
     *
     * @param username  reference user username
     * @param jogNumber sequence jog number
     * @return unique jog record
     */
    public JogRecord getByUsernameAndJogNumber(String username, Integer jogNumber) {
        Assert.notNull(jogNumber);

        log.debug("Get jog {} from user {}", jogNumber, username);
        JogRecord jogRecord = jogRecordRepository.findByUserAccountAndJogNumber(userAccountService.getByUsername(username), jogNumber);
        if (jogRecord == null) {
            throw new NoResultServiceException("Invalid jog number {0} from the user {1}", jogNumber, username);
        }
        return jogRecord;
    }

    /**
     * Search jogs for a specific user using pagination and query function to filter.
     *
     * @param username reference user username
     * @param query    custom filter
     * @param pageable requested page definition
     * @return page object with list result
     */
    public Page<JogRecord> getAllByUserAccount(String username, String query, Pageable pageable) {
        Page<JogRecord> page;
        if (!StringUtils.isEmpty(query)) {
            log.debug("Getting all jogs from user {} filtered by '{}' with page definition {}", username, query, pageable);
            page = jogRecordRepository.findAll(
                    QJogRecord.jogRecord.userAccount.eq(userAccountService.getByUsername(username)).and(
                            queryBuilder.build(query)
                    ),
                    pageable
            );
        } else {
            log.debug("Getting all jogs from user {} with page definition {}", username, pageable);
            page = jogRecordRepository.findByUserAccount(userAccountService.getByUsername(username), pageable);
        }
        return page;
    }

    /**
     * Retrieves jogs by range of dates
     *
     * @param username reference user username
     * @param fromDate start date
     * @param toDate end date
     * @return list of jogs
     */
    public Iterable<JogRecord> getByRange(String username, LocalDateTime fromDate, LocalDateTime toDate) {
        Predicate query = QJogRecord.jogRecord.userAccount.eq(userAccountService.getByUsername(username)).and(
                QJogRecord.jogRecord.date.goe(fromDate).and(
                        QJogRecord.jogRecord.date.lt(toDate.plusDays(1))
                )
        );
        return jogRecordRepository.findAll(query);
    }

    private Weather getWeather(JogRecord jogRecord) {
        Weather weather = null;
        if (jogRecord.getLocation() != null && !jogRecord.getLocation().isEmpty()) {
            weather = openWeatherClient.getWeather(jogRecord.getLocation().get(0),
                    jogRecord.getLocation().get(1));
        }
        return weather;
    }

}
