package com.voiski.toptal.service;

import com.voiski.toptal.entity.JogRecord;
import com.voiski.toptal.entity.JogReport;
import com.voiski.toptal.repository.JogRecordRepository;
import com.voiski.toptal.util.DateUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.stereotype.Service;

import java.time.LocalDate;
import java.util.concurrent.TimeUnit;

/**
 * Service to handle jog reports
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/24/16.
 */
@Slf4j
@Service
public class ReportService {

    @Autowired
    private JogRecordRepository jogRecordRepository;

    @Autowired
    private JogRecordService jogRecordService;

    @Autowired
    private UserAccountService userAccountService;

    /**
     * 1hour = 60 * 60s = 60m
     */
    private static final double ONE_HOUR = TimeUnit.HOURS.toSeconds(1);

    /**
     * 1KM = 1000M
     */
    private static final double ONE_KM = 1000D;

    /**
     * Generate a report with the average speed and distance for the reference date week. It will use all the jogs from
     * the first day of the week and the last.
     *
     * @param username      user username for reference
     * @param referenceDate reference date to get the week of the year
     * @return report with the range of dates and the average speed and distance
     */
    public JogReport getAverageSpeedDistance(String username, LocalDate referenceDate) {
        JogReport report = JogReport.builder().
                week(DateUtil.getWeekOfYear(referenceDate)).
                fromDate(DateUtil.getFirstDayOfTheWeek(referenceDate)).
                toDate(DateUtil.getLastDayOfTheWeek(referenceDate)).
                build();
        Iterable<JogRecord> jogs = jogRecordService.getByRange(
                username,
                report.getFromDate().atStartOfDay(),
                report.getToDate().atStartOfDay()
        );
        calculateAverageSpeedAndDistance(report,jogs);
        return report;
    }

    private void calculateAverageSpeedAndDistance(JogReport report, Iterable<JogRecord> jogs){
        Long totalTime = 0l;
        Long totalDistance = 0l;
        for (JogRecord jog : jogs) {
            totalTime += jog.getTime();
            totalDistance += jog.getDistance();
        }
        report.setTotalDistance(totalDistance / ONE_KM);
        report.setAverageSpeed(
                (totalDistance / ONE_KM) / (totalTime.doubleValue() / ONE_HOUR)
        );
    }
}
