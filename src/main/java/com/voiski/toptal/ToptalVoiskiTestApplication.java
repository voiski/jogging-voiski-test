package com.voiski.toptal;

import org.springframework.boot.SpringApplication;
import org.springframework.boot.autoconfigure.EnableAutoConfiguration;
import org.springframework.boot.autoconfigure.SpringBootApplication;
import org.springframework.context.annotation.ComponentScan;

/**
 * Basic spring boot starter configuration
 * 
 * @author ac-avoiski
 */
@ComponentScan
@EnableAutoConfiguration
@SpringBootApplication
public class ToptalVoiskiTestApplication {

    public static void main(String[] args) {
        SpringApplication.run(ToptalVoiskiTestApplication.class, args);
    }
}
