package com.voiski.toptal.exception;

/**
 * Exception raised in the service layer when no required result are found
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/23/16.
 */
public class NoResultServiceException extends ServiceException {

    public NoResultServiceException(String message) {
        super(message);
    }

    public NoResultServiceException(String message, Object... args) {
        super(message, args);
    }

}
