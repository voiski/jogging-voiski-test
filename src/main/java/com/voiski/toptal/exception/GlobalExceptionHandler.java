package com.voiski.toptal.exception;

import com.voiski.toptal.util.Messages;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.TypeMismatchException;
import org.springframework.core.convert.ConversionFailedException;
import org.springframework.dao.DuplicateKeyException;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.BindingResult;
import org.springframework.validation.FieldError;
import org.springframework.web.bind.MethodArgumentNotValidException;
import org.springframework.web.bind.MissingServletRequestParameterException;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.text.MessageFormat;
import java.util.ArrayList;
import java.util.List;
import java.util.regex.Matcher;
import java.util.regex.Pattern;

/**
 * Exception handler
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/23/16.
 */
@Slf4j
@ControllerAdvice
public class GlobalExceptionHandler {

    @ResponseStatus(value = HttpStatus.NOT_FOUND, reason = "IOException occurred")
    @ExceptionHandler(IOException.class)
    public void handleIOException() {
        log.info("IOException Occurred");
    }

    @ExceptionHandler(ServiceException.class)
    @ResponseBody
    public ResponseEntity<ErrorResource> handleServiceException(ServiceException se) {
        log.debug(Messages.REQUEST_FAILED, se.getMessage());
        HttpStatus httpStatus = getStatusByExceptionType(se);
        return new ResponseEntity<>(new ErrorResource(httpStatus.value(), se.getMessage()), httpStatus);
    }

    @ExceptionHandler(MethodArgumentNotValidException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public List<ErrorResource> handleMethodArgumentNotValidException(MethodArgumentNotValidException ex) {
        log.debug(Messages.REQUEST_FAILED, ex.getMessage());
        BindingResult result = ex.getBindingResult();
        List<ErrorResource> errorResources = new ArrayList<ErrorResource>();
        for (FieldError fieldError : result.getFieldErrors()) {
            errorResources.add(new ErrorResource(HttpStatus.BAD_REQUEST.value(),
                    MessageFormat.format("{0}: {1}", fieldError.getField(), fieldError.getDefaultMessage())
            ));
        }
        return errorResources;
    }

    @ExceptionHandler(TypeMismatchException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResource handleTypeMismatchException(TypeMismatchException ex) {
        log.debug(Messages.REQUEST_FAILED, ex.getMessage());
        String message = ex.getRootCause().getMessage();
        if (ex.getCause() instanceof ConversionFailedException) {
            RequestParam requestParam = ((ConversionFailedException) ex.getCause()).getTargetType()
                    .getAnnotation(RequestParam.class);
            if (requestParam != null) {
                message = MessageFormat.format("Wrong format at {0}. {1}", requestParam.value(), message);
            }
        }
        return new ErrorResource(HttpStatus.BAD_REQUEST.value(), message);
    }

    @ExceptionHandler(MissingServletRequestParameterException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResource handleMissingArgument(MissingServletRequestParameterException ex) {
        log.debug(Messages.REQUEST_FAILED, ex.getMessage());
        String message = MessageFormat.format("Missing parameter {0}.", ex.getParameterName());
        return new ErrorResource(HttpStatus.BAD_REQUEST.value(), message);
    }

    @ExceptionHandler(DuplicateKeyException.class)
    @ResponseStatus(HttpStatus.BAD_REQUEST)
    @ResponseBody
    public ErrorResource handleDuplicateKeyException(DuplicateKeyException ex) {
        log.debug(Messages.REQUEST_FAILED, ex.getMessage());
        Matcher captured = Pattern.compile("index: (\\w+)").matcher(ex.getMessage());
        String message;
        if(captured.find()){
            log.debug("Duplicated due the index {}", captured.group(1));
            message = MessageFormat.format(Messages.DUPLICATED_ENTRY, captured.group(1).replaceAll("_"," "));
        }else{
            message = "Duplicate registry";
        }
        return new ErrorResource(HttpStatus.BAD_REQUEST.value(), message);
    }

    private HttpStatus getStatusByExceptionType(ServiceException ex) {
        HttpStatus status;
        if (ex instanceof InvalidQueryException) {
            status = HttpStatus.BAD_REQUEST;
        } else if (ex instanceof NoResultServiceException) {
            status = HttpStatus.NOT_FOUND;
        } else if (ex instanceof ForbiddenFieldChangeException) {
            status = HttpStatus.FORBIDDEN;
        } else {
            status = HttpStatus.INTERNAL_SERVER_ERROR;
        }
        return status;
    }

}
