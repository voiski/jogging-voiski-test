package com.voiski.toptal.exception;

/**
 * Exception for invalid query dsl raised on the query builder phase
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/23/16.
 */
public class InvalidQueryException extends ServiceException {

    public InvalidQueryException(String message) {
        super(message);
    }

    public InvalidQueryException(String message, Object... args) {
        super(message, args);
    }

}
