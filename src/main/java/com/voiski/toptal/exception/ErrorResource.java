package com.voiski.toptal.exception;

import lombok.Value;

import java.io.Serializable;

/**
 * Error envelop to store the error code and the text message
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/23/16.
 */
@Value
public class ErrorResource implements Serializable {

    private int code;

    private String message;
}
