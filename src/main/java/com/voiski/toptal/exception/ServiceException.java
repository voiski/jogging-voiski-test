package com.voiski.toptal.exception;

import lombok.extern.slf4j.Slf4j;

import java.text.MessageFormat;

/**
 * Generic service exception, a base for the other custom exceptions
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/23/16.
 */
@Slf4j
public class ServiceException extends RuntimeException {

    public ServiceException(String message) {
        super(message);
    }

    public ServiceException(String message, Object... args) {
        this(MessageFormat.format(message, args));
    }

    public ServiceException(Exception e) {
        this(e.getMessage());
        String exceptionMessage = MessageFormat.format("Request failed due to: {0}", e.getMessage());
        log.error(exceptionMessage);
    }

}
