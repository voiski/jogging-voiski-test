package com.voiski.toptal.exception;

import com.voiski.toptal.util.Messages;

/**
 * Exception for forbidden changes
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/24/16.
 */
public class ForbiddenFieldChangeException extends ServiceException {

    public ForbiddenFieldChangeException(String field) {
        super(Messages.FORBIDDEN_FIELD_CHANGE,field);
    }

}
