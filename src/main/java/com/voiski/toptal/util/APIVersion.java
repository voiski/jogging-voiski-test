package com.voiski.toptal.util;

/**
 * API Versions
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/23/16.
 */
public interface APIVersion {
    String BASE_MIME_TYPE = "application/vnd.captech+json";
    String V1 = BASE_MIME_TYPE + "; version=1.0";

    // Example version 2
    // String V2 = BASE_MIME_TYPE + "; version=1.0";
}
