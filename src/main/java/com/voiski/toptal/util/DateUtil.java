package com.voiski.toptal.util;

import lombok.experimental.UtilityClass;

import java.sql.Timestamp;
import java.time.DayOfWeek;
import java.time.LocalDate;
import java.time.LocalDateTime;
import java.time.format.DateTimeFormatter;
import java.time.temporal.WeekFields;
import java.util.Locale;

/**
 * Utility date class to help in some common date handler tasks
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/17/16.
 */
@UtilityClass
public class DateUtil {
    public static final String PATTERN = "yyyyMMddHHmmss";
    public static final String PATTERN_DATETIME = "yyyy-MM-dd HH:mm:ss";
    public static final DateTimeFormatter FORMATTER = DateTimeFormatter.ofPattern(PATTERN);
    public static final DateTimeFormatter FORMATTER_DATETIME = DateTimeFormatter.ofPattern(PATTERN_DATETIME);

    /**
     * Base date to fill the missing parts of a incomplete date
     */
    private static final String BASE_DATE = "2016-01-01 00:00:00";

    /**
     * Parse string to a date time adding the missing parts
     */
    public static LocalDateTime parse(String date) {
        return LocalDateTime.parse(addTime(date), DateUtil.FORMATTER_DATETIME);
    }

    /**
     * Format the given string date with the full date time format adding the missing parts
     */
    public static String addTime(String date) {
        StringBuilder sb = new StringBuilder(PATTERN_DATETIME.length());
        sb.append(date);
        sb.append(BASE_DATE.substring(date.length()));
        return sb.toString();
    }

    /**
     * Retrieves the week of year from the given date
     *
     * @return week of the year
     */
    public static int getWeekOfYear(LocalDate referenceDate) {
        WeekFields weekFields = WeekFields.of(Locale.getDefault());
        return referenceDate.get(weekFields.weekOfWeekBasedYear());
    }

    /**
     * Retrieves the first day of the week from the given date
     *
     * @return first day of the week
     */
    public static LocalDate getFirstDayOfTheWeek(LocalDate referenceDate) {
        DayOfWeek weekFields = WeekFields.of(Locale.getDefault()).getFirstDayOfWeek();
        return referenceDate.with(weekFields);
    }

    /**
     * Retrieves the last day of the week from the given date
     *
     * @return last day of the week
     */
    public static LocalDate getLastDayOfTheWeek(LocalDate referenceDate) {
        return getFirstDayOfTheWeek(referenceDate).plusDays(7);
    }

    /**
     * Retrieves the first day of the month from the given date
     *
     * @return first day of the month
     */
    public static LocalDate getFirstDayOfTheMonth(LocalDate referenceDate) {
        return referenceDate.withDayOfMonth(1);
    }

    /**
     * Retrieves the last day of the month from the given date
     *
     * @return last day of the month
     */
    public static LocalDate getLastDayOfTheMonth(LocalDate referenceDate) {
        return referenceDate.withDayOfMonth(referenceDate.lengthOfMonth());
    }
}
