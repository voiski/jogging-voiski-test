package com.voiski.toptal.util;

import lombok.experimental.UtilityClass;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.BeanUtils;
import org.springframework.beans.BeanWrapper;
import org.springframework.beans.BeanWrapperImpl;

import java.beans.FeatureDescriptor;
import java.util.HashSet;
import java.util.Set;
import java.util.stream.Collectors;
import java.util.stream.Stream;

/**
 * Utility bean class to help on bean operations
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/23/16.
 */
@Slf4j
@UtilityClass
public class BeanUtil {

    /**
     * Copy the properties from one object to another ignoring null values.
     *
     * @param source       source object with the changes
     * @param target       target object to receive the new values
     * @param ignoreFields fields that should not be copied even if are not null
     */
    public static void copyNonNullProperties(Object source, Object target, String... ignoreFields) {
        Set<String> ignoredProperties = new HashSet<>();
        for (String field : ignoreFields) {
            ignoredProperties.add(field);
        }
        ignoredProperties.addAll(getNullPropertyNames(source));

        log.debug("Fields to be ignored: {}", ignoredProperties);
        BeanUtils.copyProperties(source, target, ignoredProperties.toArray(new String[0]));
    }

    /**
     * Get the properties names from the null fields.
     *
     * @param source object to be processed
     * @return set of properties names
     */
    public static Set<String> getNullPropertyNames(Object source) {
        final BeanWrapper wrappedSource = new BeanWrapperImpl(source);
        return Stream.of(wrappedSource.getPropertyDescriptors())
                .map(FeatureDescriptor::getName)
                .filter(propertyName -> wrappedSource.getPropertyValue(propertyName) == null).
                        collect(Collectors.toSet());
    }
}
