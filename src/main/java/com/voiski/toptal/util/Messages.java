package com.voiski.toptal.util;

/**
 * Constant messages
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 */
public interface Messages {

    /**
     * Message for bad password validation
     */
    String BAD_PASSWORD = "Password should contain a minimum of 8 characters, at least 1 Uppercase Alphabet, 1 Lowercase Alphabet, 1 Number and 1 Special Character";

    /**
     * Message for forbidden attempt to change a non editable field for the current role
     */
    String FORBIDDEN_FIELD_CHANGE = "You don''t have permission to change the field {0}";

    /**
     * Message for duplicated entry
     */
    String DUPLICATED_ENTRY = "Duplicated entry due the field(s) {0}";

    /**
     * Generic message for failed requests
     */
    String REQUEST_FAILED = "Request failed with: {}";
}
