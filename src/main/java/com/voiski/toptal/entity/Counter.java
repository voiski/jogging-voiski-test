package com.voiski.toptal.entity;

import lombok.Data;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.mapping.Document;

/**
 * Entity to store the sequence counter to be used as the race number by user. The id will be the username of the user account.
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/23/16.
 */
@Document
@Data
public class Counter {

    @Id
    private String id;

    private int seq;
}
