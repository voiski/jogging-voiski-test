package com.voiski.toptal.entity;

import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

/**
 * Jog weather details
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/16/16.
 */
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class Weather {
    private String description;
    private Double climate;
    private Double windSpeed;
    private Integer humidity;
}
