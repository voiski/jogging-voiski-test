package com.voiski.toptal.entity;

import lombok.experimental.UtilityClass;

/**
 * Authority roles constants
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 */
@UtilityClass
public class UserRoles {
    /**
     * Admin role have full access
     */
    public static final String ADMIN = "ADMIN";
    /**
     * Manager role have access to handle any user
     */
    public static final String MANAGER = "MANAGER";
    /**
     * User role is the default role and have access only to change the own registries
     */
    public static final String USER = "USER";
}
