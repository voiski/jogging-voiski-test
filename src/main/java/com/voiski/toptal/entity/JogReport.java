package com.voiski.toptal.entity;

import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;

import java.time.LocalDate;

/**
 * Report of jogs from a specific week of the year showing the average speed and the total traveled distance
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/24/2016.
 */
@ApiModel(description = "Report of jogs from a specific week of the year showing the average speed and the total traveled distance")
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
public class JogReport {
    @ApiModelProperty(value = "Week number of the year", example = "53")
    private Integer week;

    @ApiModelProperty(value = "Average speed in km/h", example = "12.5")
    private Double averageSpeed;

    @ApiModelProperty(value = "Total traveled distance in km", example = "25")
    private Double totalDistance;


    @ApiModelProperty(value = "First day of the week", example = "2016-12-25")
    private LocalDate fromDate;

    @ApiModelProperty(value = "Last day of the week", example = "2017-1-1")
    private LocalDate toDate;
}
