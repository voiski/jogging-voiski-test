package com.voiski.toptal.entity.validation;

/**
 * Validation group for update actions
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/23/16.
 */
public interface Update {
}
