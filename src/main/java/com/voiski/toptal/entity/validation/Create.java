package com.voiski.toptal.entity.validation;

/**
 * Validation group for create actions
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/23/16.
 */
public interface Create {
}
