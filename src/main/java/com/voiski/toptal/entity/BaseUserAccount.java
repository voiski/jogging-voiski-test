package com.voiski.toptal.entity;

import com.voiski.toptal.entity.validation.Create;
import com.voiski.toptal.entity.validation.Update;
import com.voiski.toptal.util.Messages;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Value;
import org.hibernate.validator.constraints.NotBlank;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import java.util.List;

/**
 * User account model to store the users details
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 */
@ApiModel(description = "User account")
@Builder
@AllArgsConstructor
@Value
public class BaseUserAccount {

    @ApiModelProperty(value = "User username", example = "alan")
    @NotBlank(groups = {Create.class})
    @Size(groups = {Create.class}, min = 3, max = 30)
    private String username;

    @ApiModelProperty(value = Messages.BAD_PASSWORD, example = "g00d_P4ssword")
    @NotBlank(groups = {Create.class})
    @Pattern(groups = {Create.class, Update.class},
            regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[\\W_])[A-Za-z\\d\\W_]{8,}", message = Messages.BAD_PASSWORD)
    private String password;

    @ApiModelProperty(
            value = "User roles, default to USER. Only logged users with manage or admin can set this information!",
            example = "['USER']",
            allowableValues = "USER|MANAGE|ADMIN")
    private List<String> roles;
}
