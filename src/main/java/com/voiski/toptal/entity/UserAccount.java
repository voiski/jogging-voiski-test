package com.voiski.toptal.entity;

import com.fasterxml.jackson.annotation.JsonIgnoreProperties;
import com.querydsl.core.annotations.QueryEntity;
import com.voiski.toptal.entity.validation.Create;
import com.voiski.toptal.entity.validation.Update;
import com.voiski.toptal.util.Messages;
import io.swagger.annotations.ApiModel;
import io.swagger.annotations.ApiModelProperty;
import lombok.AllArgsConstructor;
import lombok.Builder;
import lombok.Data;
import lombok.NoArgsConstructor;
import org.hibernate.validator.constraints.NotBlank;
import org.hibernate.validator.constraints.NotEmpty;
import org.springframework.data.annotation.Id;
import org.springframework.data.mongodb.core.index.IndexDirection;
import org.springframework.data.mongodb.core.index.Indexed;
import org.springframework.data.mongodb.core.mapping.Document;

import javax.validation.constraints.Pattern;
import javax.validation.constraints.Size;
import javax.validation.groups.Default;
import java.util.List;

/**
 * User account model to store the users details
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 */
@ApiModel(description = "User account")
@QueryEntity
@Document
@Builder
@AllArgsConstructor
@NoArgsConstructor
@Data
@JsonIgnoreProperties({"id","password"})
public class UserAccount {

    @Id
    private String id;

    @ApiModelProperty(value = "User username", example = "alan")
    @NotBlank(groups = {Default.class, Create.class})
    @Size(groups = {Default.class, Create.class}, min = 3, max = 30)
    @Indexed(unique = true, direction = IndexDirection.DESCENDING, dropDups = true)
    private String username;

    @ApiModelProperty(value = Messages.BAD_PASSWORD, example = "g00d_P4ssword")
    @NotBlank(groups = {Default.class, Create.class})
    @Pattern(groups = {Default.class, Create.class, Update.class},
            regexp = "^(?=.*[a-z])(?=.*[A-Z])(?=.*\\d)(?=.*[\\W_])[A-Za-z\\d\\W_]{8,}", message = Messages.BAD_PASSWORD)
    private String password;

    @ApiModelProperty(
            value = "User roles, default to USER. Only logged users with manage or admin can set this information!",
            example = "['USER']",
            allowableValues = "USER|MANAGE|ADMIN")
    @NotEmpty
    private List<String> roles;
}
