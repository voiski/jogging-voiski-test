package com.voiski.toptal.controller;

import com.voiski.toptal.entity.JogRecord;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

/**
 * Jog Record API documentation
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/25/16.
 */
@Api(
        description = "Manage jog records from the user",
        authorizations = {@Authorization(value = "basicAuth")}
)
public interface JogRecordControllerDoc {

    @ApiOperation(
            value = "Search users using pagination and query function to filter.",
            notes = "Only the admin and manage roles can use this service",
            response = Page.class
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "Invalid user {username}"),
            @ApiResponse(code = 403, message = "Access is denied, only for user role if the username is different"),
            @ApiResponse(code = 400, message =
                    "Bad criteria or bad grouping, use (field [eq|ne|gt|lt|goe|loe] 'value') for criteria and " +
                            "(criteria 1) [OR|AND] (criteria 2) for grouping"
            )
    })
    ResponseEntity<Page> index(
            @ApiParam(
                    value = "The user profile as reference",
                    required = true,
                    example = "alan")
                    String username,
            @ApiParam(
                    value = "Query dsl, use (field [eq|ne|gt|lt|goe|loe] 'value')",
                    required = true,
                    example = "query=(date eq '2016-05-01') AND ((distance gt 20) OR (distance lt 10))")
                    String query,
            @ApiParam(
                    value = "Page definition",
                    required = true,
                    example = "page=1&size=2",
                    allowableValues = "page|size",
                    defaultValue = "page=0&size=20")
                    Pageable pageable
    ) throws IOException;


    @ApiOperation(
            value = "Show details of a jog record",
            response = JogRecord.class
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "Invalid jog number {number} from the user {username}"),
            @ApiResponse(code = 403, message = "Access is denied, only for user role if the username is different")
    })
    ResponseEntity<JogRecord> show(
            @ApiParam(
                    value = "The user profile as reference",
                    required = true,
                    example = "alan")
                    String username,
            @ApiParam(
                    value = "The jog number as reference",
                    required = true,
                    example = "1")
                    Integer jogNumber
    ) throws IOException;


    @ApiOperation(
            value = "Create a new jog record",
            response = JogRecord.class
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "Invalid user {username}"),
            @ApiResponse(code = 403, message = "Access is denied, only for user role if the username is different")
    })
    ResponseEntity<JogRecord> create(
            @ApiParam(
                    value = "The user profile as reference",
                    required = true,
                    example = "alan")
                    String username,
            @ApiParam(
                    value = "The jog record to be persisted",
                    required = true)
                    JogRecord jogRecord
    ) throws IOException;


    @ApiOperation(
            value = "Update a jog record",
            response = JogRecord.class
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "Invalid jog number {number} from the user {username}"),
            @ApiResponse(code = 403, message = "Access is denied, only for user role if the username is different")
    })
    ResponseEntity<JogRecord> update(
            @ApiParam(
                    value = "The user profile as reference",
                    required = true,
                    example = "alan")
                    String username,
            @ApiParam(
                    value = "The jog number as reference",
                    required = true,
                    example = "1")
                    Integer jogNumber,
            @ApiParam(
                    value = "The jog record to be persisted",
                    required = true)
                    JogRecord jogChanges
    ) throws IOException;


    @ApiOperation(
            value = "Deletes an jog record from an user"
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "Invalid jog number {number} from the user {username}"),
            @ApiResponse(code = 403, message = "Access is denied, only for user role if the username is different")
    })
    ResponseEntity<?> delete(
            @ApiParam(
                    value = "The user profile as reference",
                    required = true,
                    example = "alan")
                    String username,
            @ApiParam(
                    value = "The jog number as reference",
                    required = true,
                    example = "1")
                    Integer jogNumber
    ) throws IOException;
}
