package com.voiski.toptal.controller;

import com.voiski.toptal.entity.JogReport;
import io.swagger.annotations.*;
import org.springframework.http.ResponseEntity;

import java.io.IOException;
import java.time.LocalDate;

/**
 * Report API documentation
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/25/16.
 */
@Api(
        description = "Generate reports",
        authorizations = {@Authorization(value = "basicAuth")}
)
public interface ReportControllerDoc {

    @ApiOperation(
            value = "Report of jogs from a specific week of the year",
            notes = "Report of jogs from a specific week of the year showing the average speed and the total traveled distance.",
            response = JogReport.class
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "Invalid user {username}"),
            @ApiResponse(code = 403, message = "Access is denied, only for user role if the username is different")
    })
    ResponseEntity<JogReport> week(
            @ApiParam(
                    value = "The user profile as reference",
                    required = true,
                    example = "alan")
                    String username,
            @ApiParam(
                    value = "A date for reference, the report will use it to ge the week of the year. When empty it will use the current server day.",
                    required = false,
                    example = "2016-12-25")
                    LocalDate referenceDate
    ) throws IOException;
}
