package com.voiski.toptal.controller;

import com.voiski.toptal.entity.JogRecord;
import com.voiski.toptal.entity.validation.Create;
import com.voiski.toptal.entity.validation.Update;
import com.voiski.toptal.service.JogRecordService;
import com.voiski.toptal.util.APIVersion;
import com.voiski.toptal.util.BeanUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;

/**
 * Jog controller to expose the CRUD endpoints
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/16/16.
 */
@Slf4j
@RestController
@RequestMapping(path = "/user/{username}/jog", produces = APIVersion.V1)
public class JogRecordController implements JogRecordControllerDoc {

    @Autowired
    private JogRecordService jogRecordService;

    @Override
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Page> index(@PathVariable("username") String username,
                                      @RequestParam(value = "query", required = false) String query,
                                      Pageable pageable) throws IOException {
        log.info("Retrieving page {} jogs from the user: {}", pageable.getPageNumber(), username);
        return new ResponseEntity<>(jogRecordService.getAllByUserAccount(username, query, pageable), HttpStatus.OK);
    }

    @Override
    @RequestMapping(path = "/{jogNumber}", method = RequestMethod.GET)
    public ResponseEntity<JogRecord> show(@PathVariable("username") String username,
                                          @PathVariable("jogNumber") Integer jogNumber) throws IOException {
        log.info("Retrieving jog record {} from user {}", jogNumber, username);
        return new ResponseEntity<>(jogRecordService.getByUsernameAndJogNumber(username, jogNumber), HttpStatus.OK);
    }

    @Override
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<JogRecord> create(@PathVariable("username") String username,
                                            @Validated(Create.class) @RequestBody JogRecord jogRecord)
            throws IOException {
        log.info("Adding new jog record ({}) to user: {}", jogRecord.getDate(), username);
        jogRecordService.create(username, jogRecord);
        return new ResponseEntity<>(jogRecord, HttpStatus.CREATED);
    }

    @Override
    @RequestMapping(path = "/{jogNumber}", method = RequestMethod.PUT)
    public ResponseEntity<JogRecord> update(@PathVariable("username") String username,
                                    @PathVariable("jogNumber") Integer jogNumber,
                                    @Validated(Update.class) @RequestBody JogRecord jogChanges) throws IOException {
        log.info("Updating jog record {} from user: {}", jogNumber, username);
        JogRecord jogRecord = jogRecordService.getByUsernameAndJogNumber(username, jogNumber);
        BeanUtil.copyNonNullProperties(jogChanges, jogRecord, "id", "userAccount");
        jogRecordService.save(username, jogRecord);
        return new ResponseEntity<>(jogRecord, HttpStatus.OK);
    }

    @Override
    @RequestMapping(path = "/{jogNumber}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable("username") String username,
                                    @PathVariable("jogNumber") Integer jogNumber) throws IOException {
        log.info("Deleting jog record {} from user account: {}", jogNumber, username);
        jogRecordService.delete(jogRecordService.getByUsernameAndJogNumber(username, jogNumber));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }
}
