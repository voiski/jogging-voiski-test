package com.voiski.toptal.controller;

import com.voiski.toptal.entity.BaseUserAccount;
import com.voiski.toptal.entity.UserAccount;
import com.voiski.toptal.util.Messages;
import io.swagger.annotations.*;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.ResponseEntity;

import java.io.IOException;

/**
 * User Account API documentation
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/25/16.
 */
@Api(
        description = "Manage user account",
        authorizations = {@Authorization(value = "basicAuth")}
)
public interface UserAccountControllerDoc {
    @ApiOperation(
            value = "Search users using pagination and query function to filter.",
            notes = "Only the admin and manage roles can use this service",
            response = Page.class
    )
    @ApiResponses({
            @ApiResponse(code = 400, message =
                    "Bad criteria or bad grouping, use (field [eq|ne|gt|lt|goe|loe] 'value') for criteria and " +
                            "(criteria 1) [OR|AND] (criteria 2) for grouping"
            ),
            @ApiResponse(code = 403, message = "Access is denied, only for user role if the username is different")
    })
    ResponseEntity<Page> index(
            @ApiParam(
                    value = "Query dsl, use (field [eq|ne|gt|lt|goe|loe] 'value')",
                    required = true,
                    example = "query=(username eq 'alan')")
                    String query,
            @ApiParam(
                    value = "Page definition",
                    required = true,
                    example = "page=1&size=2",
                    allowableValues = "page|size",
                    defaultValue = "page=0&size=20")
                    Pageable pageable
    ) throws IOException;


    @ApiOperation(
            value = "Show details of an user account",
            response = UserAccount.class
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "Invalid user {username}"),
            @ApiResponse(code = 403, message = "Access is denied, only for user role if the username is different")
    })
    ResponseEntity<UserAccount> show(
            @ApiParam(
                    value = "The user profile as reference",
                    required = true,
                    example = "alan")
                    String username
    ) throws IOException;


    @ApiOperation(
            value = "Create a new user account",
            notes = "Only the manage and the admin roles can set the roles list.",
            response = UserAccount.class
    )
    @ApiResponses({
            @ApiResponse(code = 400, message = "Duplicated due the index username or " + Messages.BAD_PASSWORD),
            @ApiResponse(code = 403, message = "You don't have permission to change the field roles")
    })
    ResponseEntity<UserAccount> create(
            @ApiParam(
                    value = "The user account containing the username and the password",
                    required = true)
                    BaseUserAccount userAccount
    ) throws IOException;


    @ApiOperation(
            value = "Update an user account",
            notes = "Only the manage and the admin roles can set the roles list.",
            response = UserAccount.class
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "Invalid user {username}"),
            @ApiResponse(code = 400, message = "Duplicated due the index username or " + Messages.BAD_PASSWORD),
            @ApiResponse(code = 403, message = "Access is denied, only for user role if the username is different." +
                    " I can happens also when the user doesn't have permission to change the field roles."
            )
    })
    ResponseEntity<UserAccount> update(
            @ApiParam(
                    value = "The user profile as reference",
                    required = true,
                    example = "alan")
                    String username,
            @ApiParam(
                    value = "The user account containing new values to be changed",
                    required = true)
                    BaseUserAccount userChanges
    ) throws IOException;


    @ApiOperation(
            value = "Deletes an user account",
            response = UserAccount.class
    )
    @ApiResponses({
            @ApiResponse(code = 404, message = "Invalid user {username}"),
            @ApiResponse(code = 403, message = "Access is denied, only for user role if the username is different")
    })
    ResponseEntity<?> delete(
            @ApiParam(
                    value = "The user profile as reference",
                    required = true,
                    example = "alan")
                    String username
    ) throws IOException;
}
