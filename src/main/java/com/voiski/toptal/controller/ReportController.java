package com.voiski.toptal.controller;

import com.voiski.toptal.entity.JogReport;
import com.voiski.toptal.service.ReportService;
import com.voiski.toptal.util.APIVersion;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.format.annotation.DateTimeFormat;
import org.springframework.http.HttpStatus;
import org.springframework.http.MediaType;
import org.springframework.http.ResponseEntity;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.time.LocalDate;

/**
 * Report controller to expose the jog reports
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/24/16.
 */
@Slf4j
@RestController
@RequestMapping(path = "/user/{username}/report", produces = APIVersion.V1)
public class ReportController implements ReportControllerDoc {

    @Autowired
    private ReportService reportService;

    @RequestMapping(path = "/week", method = RequestMethod.GET)
    public ResponseEntity<JogReport> week(@PathVariable("username") String username,
                                          @RequestParam(name = "date", required = false)
                                          @DateTimeFormat(pattern = "yyyy-MM-dd")
                                                  LocalDate referenceDate) throws IOException {
        log.info("Generating report for the user: {}", username);
        if (referenceDate == null) {
            referenceDate = LocalDate.now();
        }
        return new ResponseEntity<>(reportService.getAverageSpeedDistance(username, referenceDate), HttpStatus.OK);
    }


}
