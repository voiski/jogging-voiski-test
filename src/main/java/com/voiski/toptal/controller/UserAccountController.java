package com.voiski.toptal.controller;

import com.voiski.toptal.entity.BaseUserAccount;
import com.voiski.toptal.entity.UserAccount;
import com.voiski.toptal.entity.UserRoles;
import com.voiski.toptal.entity.validation.Create;
import com.voiski.toptal.entity.validation.Update;
import com.voiski.toptal.exception.ForbiddenFieldChangeException;
import com.voiski.toptal.service.UserAccountService;
import com.voiski.toptal.util.APIVersion;
import com.voiski.toptal.util.BeanUtil;
import lombok.extern.slf4j.Slf4j;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.domain.Page;
import org.springframework.data.domain.Pageable;
import org.springframework.http.HttpStatus;
import org.springframework.http.ResponseEntity;
import org.springframework.security.core.GrantedAuthority;
import org.springframework.security.core.context.SecurityContextHolder;
import org.springframework.util.CollectionUtils;
import org.springframework.validation.annotation.Validated;
import org.springframework.web.bind.annotation.*;

import java.io.IOException;
import java.util.Arrays;
import java.util.Collection;
import java.util.stream.Collectors;

/**
 * User controller to expose the CRUD endpoints
 *
 * @author Alan Voiski <alannunesv@gmail.com>
 */
@Slf4j
@RestController
@RequestMapping(path = "/user", produces = APIVersion.V1)
public class UserAccountController implements UserAccountControllerDoc {

    @Autowired
    private UserAccountService userAccountService;

    @Override
    @RequestMapping(method = RequestMethod.GET)
    public ResponseEntity<Page> index(@RequestParam(value = "query", required = false) String query, Pageable pageable) throws IOException {
        log.info("Retrieving user account list: [query:{},page:{}]", query, pageable);
        return new ResponseEntity<>(userAccountService.getAll(query, pageable), HttpStatus.OK);
    }

    @Override
    @RequestMapping(path = "/{username}", method = RequestMethod.GET)
    public ResponseEntity<UserAccount> show(@PathVariable("username") String username) throws IOException {
        log.info("Retriving user account: {}", username);
        return new ResponseEntity<>(userAccountService.getByUsername(username), HttpStatus.OK);
    }

    @Override
    @RequestMapping(method = RequestMethod.POST)
    public ResponseEntity<UserAccount> create(@Validated(Create.class) @RequestBody BaseUserAccount userChanges)
            throws IOException {
        log.info("Adding new user account: {}", userChanges.getUsername());
        validateChange(userChanges);
        UserAccount userAccount = UserAccount.builder().
                username(userChanges.getUsername()).
                password(userChanges.getPassword()).
                roles(userChanges.getRoles()).
                build();
        userAccountService.create(userAccount);
        return new ResponseEntity<>(userAccount, HttpStatus.CREATED);
    }

    @Override
    @RequestMapping(path = "/{username}", method = RequestMethod.PUT)
    public ResponseEntity<UserAccount> update(@PathVariable("username") String username,
                                              @Validated(Update.class) @RequestBody BaseUserAccount userChanges) throws IOException {
        log.info("Updating user account: {}", username);
        validateChange(userChanges);
        UserAccount userAccount = userAccountService.getByUsername(username);
        BeanUtil.copyNonNullProperties(userChanges, userAccount, "id");
        userAccountService.save(userAccount);
        return new ResponseEntity<>(userAccount, HttpStatus.OK);
    }

    @Override
    @RequestMapping(path = "/{username}", method = RequestMethod.DELETE)
    public ResponseEntity<?> delete(@PathVariable("username") String username) throws IOException {
        log.info("Deleting user account: {}", username);
        userAccountService.delete(userAccountService.getByUsername(username));
        return new ResponseEntity<>(HttpStatus.NO_CONTENT);
    }

    private void validateChange(BaseUserAccount userAccount) {
        // TODO: update the tests or create more
        if (!hasRole(UserRoles.ADMIN, UserRoles.MANAGER) &&
                !CollectionUtils.isEmpty(userAccount.getRoles())) {
            throw new ForbiddenFieldChangeException("roles");
        }
    }

    private boolean hasRole(String... roles) {
        Collection<String> authorities = Arrays.stream(roles).
                map("ROLE_"::concat).
                collect(Collectors.toSet());
        boolean hasRole = false;
        for (GrantedAuthority authority : SecurityContextHolder.getContext().getAuthentication().getAuthorities()) {
            if (authorities.contains(authority.getAuthority())) {
                hasRole = true;
                break;
            }
        }
        return hasRole;
    }
}
