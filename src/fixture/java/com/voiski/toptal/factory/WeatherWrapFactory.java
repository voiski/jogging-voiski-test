package com.voiski.toptal.factory;

import com.voiski.toptal.client.wrap.WeatherDetailsWrap;
import com.voiski.toptal.client.wrap.WeatherMainWrap;
import com.voiski.toptal.client.wrap.WeatherWindWrap;
import com.voiski.toptal.client.wrap.WeatherWrap;
import com.voiski.toptal.client.wrap.WeatherWrap.WeatherWrapBuilder;
import lombok.experimental.UtilityClass;

import java.util.Collections;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/16/16.
 */
@UtilityClass
public class WeatherWrapFactory {

    public WeatherWrap valid() {
        return builder().build();
    }

    public WeatherWrapBuilder builder() {
        return WeatherWrap.builder().
                weather(Collections.singletonList(WeatherDetailsWrap.builder().description("Clear").build())).
                main(WeatherMainWrap.builder().humidity(10).temp(230.1).build()).
                wind(WeatherWindWrap.builder().speed(2.3).build());
    }
}
