package com.voiski.toptal.factory;

import com.querydsl.core.types.dsl.*;

import java.time.LocalDateTime;

import static com.querydsl.core.types.PathMetadataFactory.forVariable;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/21/16.
 */
public class EntityPathFactory extends EntityPathBase<Object> {

    public static final EntityPathFactory base = new EntityPathFactory("base");

    public final StringPath stringPath = createString("string");
    public final DateTimePath<LocalDateTime> datePath = createDateTime("date", java.time.LocalDateTime.class);

    public final NumberPath<Integer> integerPath = createNumber("integer", Integer.class);
    public final NumberPath<Double> doublePath = createNumber("double", Double.class);
    public final NumberPath<Long> longPath = createNumber("long", Long.class);

    public final ListPath<Double, NumberPath<Double>> listPath = this.<Double, NumberPath<Double>>createList("location", Double.class, NumberPath.class, PathInits.DIRECT2);

    public EntityPathFactory(String variable) {
        super(Object.class, forVariable(variable), PathInits.DIRECT2);
    }


}
