package com.voiski.toptal.factory;

import com.voiski.toptal.entity.BaseUserAccount;
import com.voiski.toptal.entity.UserAccount;
import com.voiski.toptal.entity.UserAccount.UserAccountBuilder;
import com.voiski.toptal.entity.UserRoles;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.util.Arrays;
import java.util.Collections;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/15/16.
 */
@Component
public class UserAccountFactory {

    private static int sequence = 1;

    @Autowired
    private MongoTemplate mongoTemplate;

    public static UserAccount validNew() {
        return builder().build();
    }

    public static UserAccount valid() {
        return builder().id("id" + (sequence - 1)).build();
    }

    public UserAccount user() {
        return byRole(UserRoles.USER);
    }

    public UserAccount manager() {
        return byRole(UserRoles.MANAGER);
    }

    public UserAccount admin() {
        return byRole(UserRoles.ADMIN);
    }

    public UserAccount byRole(String... roles) {
        UserAccount user = builder().roles(Arrays.asList(roles)).build();
        if (mongoTemplate != null) {
            mongoTemplate.save(user);
        } else {
            user.setId("id" + sequence++);
        }
        return user;
    }

    public UserAccountBuilder builder(UserAccount example) {
        return UserAccount.builder()
                .id(example.getId())
                .username(example.getUsername())
                .password(example.getPassword())
                .roles(example.getRoles());
    }

    public void clearAll() {
        mongoTemplate.findAllAndRemove(new Query(), UserAccount.class);
    }

    public static UserAccountBuilder builder() {
        return UserAccount.builder()
                .username("alan_" + sequence++)
                .password("g00d_P4ssword")
                .roles(Collections.singletonList(UserRoles.USER));
    }

    public static BaseUserAccount toBase(UserAccount userAccount) {
        return BaseUserAccount.builder().
                username(userAccount.getUsername()).
                password(userAccount.getPassword()).
                roles(userAccount.getRoles()).
                build();
    }
}
