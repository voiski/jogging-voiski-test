package com.voiski.toptal.factory;

import com.voiski.toptal.entity.JogReport;
import com.voiski.toptal.entity.JogReport.JogReportBuilder;
import org.springframework.stereotype.Component;

import java.time.LocalDate;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/15/16.
 */
@Component
public class ReportFactory {

    public static JogReport valid() {
        return builder().build();
    }

    public JogReportBuilder builder(JogReport example) {
        return JogReport.builder()
                .week(example.getWeek())
                .fromDate(example.getFromDate())
                .toDate(example.getToDate())
                .totalDistance(example.getTotalDistance())
                .averageSpeed(example.getAverageSpeed());
    }

    public static JogReportBuilder builder() {
        return JogReport.builder()
                .week(1)
                .fromDate(LocalDate.of(2016, 1, 1))
                .toDate(LocalDate.of(2016, 1, 31))
                .totalDistance(10D)
                .averageSpeed(12d);
    }
}
