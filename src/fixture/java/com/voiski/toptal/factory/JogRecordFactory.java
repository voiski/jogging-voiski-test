package com.voiski.toptal.factory;

import com.voiski.toptal.entity.JogRecord;
import com.voiski.toptal.entity.JogRecord.JogRecordBuilder;
import com.voiski.toptal.entity.UserAccount;
import com.voiski.toptal.entity.Weather;
import org.springframework.beans.factory.annotation.Autowired;
import org.springframework.data.mongodb.core.MongoTemplate;
import org.springframework.data.mongodb.core.query.Query;
import org.springframework.stereotype.Component;

import java.time.Duration;
import java.time.LocalDateTime;
import java.util.Arrays;

/**
 * @author Alan Voiski <alannunesv@gmail.com>
 * @since 12/15/16.
 */
@Component
public class JogRecordFactory {

    private static int sequence = 1;

    @Autowired
    private MongoTemplate mongoTemplate;

    @Autowired
    private UserAccountFactory userAccountFactory;

    public static JogRecord validNew() {
        return builder().build();
    }

    public static JogRecord valid() {
        return builder().id("id" + (sequence - 1)).build();
    }

    public JogRecord today() {
        return today(userAccountFactory.user());
    }

    public JogRecord today(UserAccount userAccount) {
        return byDate(userAccount, LocalDateTime.now());
    }

    public JogRecord pastDays(UserAccount userAccount, int days) {
        return byDate(userAccount, LocalDateTime.now().minusDays(days));
    }

    public JogRecord byDate(UserAccount userAccount, LocalDateTime date) {
        return build(builder().userAccount(userAccount).date(date));
    }

    public JogRecord build(JogRecordBuilder builder) {
        JogRecord jog = builder.jogNumber(sequence++).build();
        if (mongoTemplate != null) {
            mongoTemplate.save(jog);
        } else {
            jog.setId("id" + jog.getJogNumber());
        }
        return jog;

    }

    public JogRecordBuilder builder(JogRecord example) {
        return JogRecord.builder()
                .id(example.getId())
                .jogNumber(example.getJogNumber())
                .userAccount(example.getUserAccount())
                .distance(example.getDistance())
                .time(example.getTime())
                .date(example.getDate())
                .location(example.getLocation())
                .weather(example.getWeather());
    }

    public void clearAll() {
        mongoTemplate.findAllAndRemove(new Query(), JogRecord.class);
    }

    public static JogRecordBuilder builder() {
        return JogRecord.builder()
                .jogNumber(sequence++)
                .userAccount(UserAccountFactory.valid())
                .distance(10000)
                .time(Duration.ofHours(1).getSeconds())
                .date(LocalDateTime.now().withNano(0))
                .location(Arrays.asList(-19.9410036, -43.9770468))
                .weather(
                        Weather.builder().
                                description("few clouds").
                                climate(293.654).
                                windSpeed(2.93).
                                humidity(87).
                                build()
                );
    }
}
